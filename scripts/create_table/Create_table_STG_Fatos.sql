
/*CRIACAO DAS TABELAS DO DW UNILEVER*
**TABELAS DE DIMENS�O E FATO*********
**DATA:04/12/2019********************
**AUTOR: BRUNO ANDRADE**************/

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/********************
**[dw].[FAT_ESTOQUE]*
********************/



USE [ADD_STAGING] 
GO

/****** Object:  Table [dw].[FAT_ESTOQUE]    Script Date: 26/11/2019 17:42:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE  [bi_b2b].[STG_FAT_ESTOQUE](
	[SK_ESTOQUE] [int],
	[SK_PRODUTO] [int],
	[SK_LOJA] [int],
	[SK_DATA] [int],
	[SK_DISTRIBUIDOR] [int],
	[SK_TIPO_ESTOQUE] [int],
	[QTD_ESTOQUE] [int])


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da Tabela' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_ESTOQUE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave primaria da tabela de Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave primaria da tabela de Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave primaria da tabela de Data' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave primaria da tabela de Distribuidor' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave primaria da tabela de Tipo de Estoque' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_TIPO_ESTOQUE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade do Estoque' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'QTD_ESTOQUE'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*****************************
*[dw].[FAT_LIMITE_DE_CREDITO]*
****************************/

USE [ADD_STAGING] 
GO

/****** Object:  Table [dw].[FAT_LIMITE_DE_CREDITO]    Script Date: 04/12/2019 11:10:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE  [bi_b2b].[STG_FAT_LIMITE_DE_CREDITO](
	[SK_LIMITE_DE_CREDITO] [int],
	[SK_CLIENTE] [int],
	[SK_DISTRIBUIDOR] [int],
	[SK_CLIENTE_DISTRIBUIDOR] [int],
	[SK_DATA] [int],
	[SK_LOJA] [int],
	[SK_STATUS_CLIENTE_DISTRIBUIDOR] [int],
	[VLR_LIMITE_DE_CREDITO] [numeric](10, 2))


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_LIMITE_DE_CREDITO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente Distribuidor' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Status Cliente Distribuidor' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_STATUS_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do limite de Credito , Cliente por Distribuidor' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'VLR_LIMITE_DE_CREDITO'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*********************
*[dw].[FAT_PAGAMENTO]*
*********************/

USE [ADD_STAGING] 
GO

/****** Object:  Table [dw].[FAT_PAGAMENTO]    Script Date: 04/12/2019 11:21:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE  [bi_b2b].[STG_FAT_PAGAMENTO](
	[SK_PAGAMENTO] [int],
	[SK_PEDIDO] [int],
	[SK_TIPO_PAGAMENTO] [int],
	[SK_LOJA] [int],
	[SK_DATA] [int],
	[VALOR_POR_TIPO_PAGAMENTO] [numeric](10, 2)NOT NULL)


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'SK_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tipo Pagamento' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'SK_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Pagor por Tipo de pagamento no pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'VALOR_POR_TIPO_PAGAMENTO'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*******************************
*[dw].[FAT_PEDIDO_CAPTADO_ITEM]*
*******************************/

USE [ADD_STAGING]
GO

/****** Object:  Table [bi_b2b].[STG_FAT_PEDIDO_CAPTADO_ITEM]    Script Date: 16/12/2019 10:40:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [bi_b2b].[STG_FAT_PEDIDO_CAPTADO_ITEM](
	[SK_PEDIDO_CAPTADO_ITEM] [int] NULL,
	[SK_PEDIDO] [int] NULL,
	[SK_PRODUTO] [int] NULL,
	[SK_CLIENTE] [int] NULL,
	[SK_DATA] [int] NULL,
	[SK_LOJA] [int] NULL,
	[SK_KIT] [int] NULL,
	[SK_ENDERECO] [int] NULL,
	[SK_DISTRIBUIDOR] [int] NULL,
	[SK_CANAL_VENDA] [int] NULL,
	[SK_TIPO_PAGAMENTO] [int] NULL,
	[SK_CLIENTE_DISTRIBUIDOR] [int] NULL,
	[SK_FRONT] [int] NULL,
	[QTD_ITEM] [int] NULL,
	[VLR_ITEM] [numeric](10, 2) NULL,
	[VLR_DESCONTO] [numeric](10, 2) NULL,
	[VLR_PEDIDO_REAL] [numeric](10, 2) NULL,
	[VLR_MIDDLEWARE] [numeric](10, 2) NULL
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO_CAPTADO_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Kit' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_KIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Endere�o' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_ENDERECO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Canal de Venda' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_CANAL_VENDA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tipo de Pagamento' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Front' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_FRONT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade do item por Pedido' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'QTD_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Item' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'VLR_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do desconto do Pedido por item' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'VLR_DESCONTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Real do Pedido por Item' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'VLR_PEDIDO_REAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Middleware' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'VLR_MIDDLEWARE'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/**************************
*[dw].[FAT_PEDIDO_DEV_EXT]*
**************************/

USE [ADD_STAGING] 
GO

/****** Object:  Table [dw].[FAT_PEDIDO_DEV_EXT]    Script Date: 04/12/2019 12:39:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE  [bi_b2b].[STG_FAT_PEDIDO_DEV_EXT](
	[SK_PEDIDO_DEV_EXT] [int],
	[SK_PRODUTO] [int],
	[SK_KIT] [int],
	[SK_CLIENTE_DISTRIBUIDOR] [int],
	[SK_FRONT] [int],
	[SK_DATA] [int],
	[SK_CANAL_VENDA] [int],
	[SK_ENDERECO] [int],
	[SK_PEDIDO] [int],
	[SK_CLIENTE] [int],
	[SK_LOJA] [int],
	[SK_DISTRIBUIDOR] [int],
	[SK_TIPO_PAGAMENTO] [int],
	[QTD_ITEM] [int],
	[VLR_ITEM] [numeric](10, 2),
	[VLR_DESCONTO] [numeric](10, 2),
	[VLR_PEDIDO_REAL] [numeric](10, 2),
	[VLR_TOTAL_NF] [numeric](10, 2),
	[VLR_TOTAL_PEDIDO] [numeric](10, 2),
	[VLR_ICMS_ST] [numeric](10, 2),
	[VLR_ICMS] [numeric](10, 2),
	[VLR_PIS] [numeric](10, 2),
	[VLR_COFINS] [numeric](10, 2))


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Kit' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_KIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Canal Venda' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_CANAL_VENDA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Endere�o' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_ENDERECO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tipo Pagamento' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de Itens  Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'QTD_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Desconto por Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_DESCONTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Real do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_PEDIDO_REAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Total da Nota Fiscal do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_TOTAL_NF'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Total do Pedido do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_TOTAL_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor de ICMS_ST do  Pedido do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_ICMS_ST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor de ICMS_ST do  Pedido do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_ICMS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do PIS do Pedido do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_PIS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Cofins do Pedido do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_COFINS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente Distribuidor' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Front' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_FRONT'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE [ADD_STAGING]
GO

/***** Object:  Table [bi_b2b].[STG_FAT_PEDIDO_FATURADO]    Script Date: 04/12/2019 13:08:53 *****/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [bi_b2b].[STG_FAT_PEDIDO_FATURADO](
	[SK_PEDIDO] [int],
	[SK_NOTA_FISCAL] [int],
	[SK_PRODUTO] [int],
	[SK_PROMOCAO] [int],
	[SK_DATA] [int],
	[SK_CANAL_DE_VENDA] [int],
	[SK_ENDERECO] [int],
	[SK_CLIENTE] [int],
	[SK_LOJA] [int],
	[SK_DISTRIBUIDOR] [int],
	[SK_TIPO_PAGAMENTO] [int],
	[QTD_ITEM] [int],
	[VLR_ITEM] [numeric](10, 2),
	[VLR_DESCONTO] [numeric](10, 2),
	[VLR_PEDIDO_REAL] [numeric](10, 2),
	[VLR_TOTAL_NF] [numeric](10, 2),
	[VLR_TOTAL_PEDIDO] [numeric](10, 2),
	[VLR_ICMS_ST] [numeric](10, 2),
	[VLR_ICMS] [numeric](10, 2),
	[VLR_PIS] [numeric](10, 2),
	[VLR_COFINS] [numeric](10, 2),
	[FATOR_QTD] [numeric](10, 4),
	[QTD_BONIFICADA] [int],
	[QTD_BONIFICADA_FAT] [int],
	[PCT_BONIFICADA_FAT] [int],
	[QTD_CAVALO] [int],
	[QTD_CAVALO_FAT] [int],
	[PCT_CAVALO_FAT] [int],
	[FATOR_BONIFICADO] [int])



EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Nota Fiscal' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_NOTA_FISCAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Promo��o' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_PROMOCAO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Canal Venda' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_CANAL_DE_VENDA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela  Endereco' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_ENDERECO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tipo Pagamento' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de item Faturado por Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'QTD_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor de item Faturado por Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Desconto Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_DESCONTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Real  Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_PEDIDO_REAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Total da Nota Fiscal  Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_TOTAL_NF'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Total do Pedido do Item  Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_TOTAL_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Total do Pedido do Item  Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_ICMS_ST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor de ICMS do Item Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_ICMS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor de ICMS do Item Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_PIS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Cofins do Item Faturado por Produto' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_COFINS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fator de pondera��o da verba aplicada aos produtos n�o bonificado' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'FATOR_QTD'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'� a mesma informa��o da coluna da tabela ifc TBBR_ORDER_LINE_PROMOTION_BI, por�m em outra granularidade e na tabela ECAD_ORDER_LINE' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'QTD_BONIFICADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de unidades bonificadas que foram faturadas' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'QTD_BONIFICADA_FAT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Percentual das unidades bonificadas que foram faturadas' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'PCT_BONIFICADA_FAT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de unidades que s�o cavalos' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'QTD_CAVALO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de unidades cavalos que foram faturadas' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'QTD_CAVALO_FAT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Percentual dos Cavalos que foi faturado' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'PCT_CAVALO_FAT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fator de pondera��o da verba aplicada aos produtos bonificados' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'FATOR_BONIFICADO'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/**********************************
*[dw].[STG_FAT_PEDIDO_STATUS_RASTREIO]*
**********************************/

USE [ADD_STAGING] 
GO

/****** Object:  Table [dw].[STG_FAT_PEDIDO_STATUS_RASTREIO]    Script Date: 22/11/2019 18:33:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE  [bi_b2b].[STG_FAT_PEDIDO_STATUS_RASTREIO](
	[SK_PEDIDO_STATUS_RASTREIO] [int],
	[SK_DATA] [int],
	[SK_STATUS] [int],
	[SK_LOJA] [int],
	[SK_DISTRIBUIDOR] [int],
	[SK_PEDIDO] [int],
	[SK_NOTA_FISCAL] [int])


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tempo' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Status' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_STATUS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Nota Fiscal' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_NOTA_FISCAL'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*****************************
*[dw].[FAT_PRODUTO_HISTORICO]*
*****************************/

USE [ADD_STAGING] 
GO

/****** Object:  Table [dw].[FAT_PRODUTO_HISTORICO]    Script Date: 26/11/2019 18:05:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE  [bi_b2b].[STG_FAT_PRODUTO_HISTORICO](
	[SK_PRODUTO_HISTORICO] [int],
	[SK_PRODUTO] [int],
	[SK_DISTRIBUIDOR] [int],
	[SK_DATA] [int],
	[SK_LOJA] [int],
	[PRECO_DE] [numeric](10, 2),
	[PRECO_POR] [numeric](10, 2))

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO_HISTORICO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Preco De do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'PRECO_DE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Preco Por do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'PRECO_POR'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*****************************
*[dw].[FAT_PROMOCAO_APLICADA]*
*****************************/

USE [ADD_STAGING] 
GO

/****** Object:  Table [dw].[FAT_PROMOCAO_APLICADA]    Script Date: 04/12/2019 13:16:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE  [bi_b2b].[STG_FAT_PROMOCAO_APLICADA](
	[SK_PROMOCAO_APLICADA] [int],
	[SK_PEDIDO] [int],
	[SK_PRODUTO] [int],
	[SK_REGRA_PROMOCAO] [int],
	[SK_DATA] [int],
	[SK_KIT] [int],
	[SK_LOJA] [int],
	[VLR_PROMO_ITEM] [numeric](10, 2),
	[QTD_PROMO_APLICADA] [int],
	[QTD_MIN_PROMO] [int],
	[QTD_ITEM_TOTAL] [int],
	[VLR_PROMO_APLICADA] [numeric](10, 2),
	[QTD_BONIFICADA] [int],
	[PROMO_BONIFICADA] [int])

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_PROMOCAO_APLICADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Regra de Promocao' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_REGRA_PROMOCAO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Kit' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_KIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valoda promo��o do item' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'VLR_PROMO_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de itens com a promocao aplicada' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'QTD_PROMO_APLICADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade minima de itens para a promocao' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'QTD_MIN_PROMO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade total de itens' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'QTD_ITEM_TOTAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor da Promocao aplicada' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'VLR_PROMO_APLICADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade bonificada' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'QTD_BONIFICADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Promocao Bonificada' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'PROMO_BONIFICADA'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*****************************
*[dw].[FAT_PROMOCAO_FATURADA]*
*****************************/

USE [ADD_STAGING] 
GO

/****** Object:  Table [dw].[FAT_PROMOCAO_FATURADA]    Script Date: 04/12/2019 13:23:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE  [bi_b2b].[STG_FAT_PROMOCAO_FATURADA](
	[SK_PROMOCAO_FATURADA] [int],
	[SK_PEDIDO] [int],
	[SK_KIT] [int],
	[SK_PRODUTO] [int],
	[SK_LOJA] [int],
	[SK_TIPO_PAGAMENTO] [int],
	[SK_DATA] [int],
	[VALOR_ITEM_PROMO_FATURADO] Numeric (10,2))

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_PROMOCAO_FATURADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela KIT' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_KIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tipo Pagamento' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Faturado da Promocao por Pedido item' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'VALOR_ITEM_PROMO_FATURADO'
GO


