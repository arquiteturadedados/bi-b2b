
/*CRIACAO DAS TABELAS DO DW UNILEVER*
**TABELAS DE DIMENSÃO E FATO*********
**DATA:04/12/2019********************
**AUTOR: BRUNO ANDRADE**************/



/***********************
*[dw].[DIM_CANAL_VENDA]*
***********************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_CANAL_VENDA]    Script Date: 22/11/2019 14:34:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_CANAL_VENDA](
	[SK_CANAL_VENDA] [int] NOT NULL,
	[CD_CANAL_VENDA] [int] NOT NULL,
	[DS_CANAL_VENDA] [varchar](50) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_CANAL_VENDA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_CANAL_VENDA] ADD  DEFAULT ((-2)) FOR [CD_CANAL_VENDA]
GO

ALTER TABLE [dw].[DIM_CANAL_VENDA] ADD  DEFAULT ('-2') FOR [DS_CANAL_VENDA]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CANAL_VENDA', @level2type=N'COLUMN',@level2name=N'SK_CANAL_VENDA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo do Canal de Venda' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CANAL_VENDA', @level2type=N'COLUMN',@level2name=N'CD_CANAL_VENDA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao Canal De Venda' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CANAL_VENDA', @level2type=N'COLUMN',@level2name=N'DS_CANAL_VENDA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CANAL_VENDA', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CANAL_VENDA', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*******************
*[dw].[DIM_CLIENTE]*
*******************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_CLIENTE]    Script Date: 03/12/2019 18:41:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_CLIENTE](
	[SK_CLIENTE] [int] NOT NULL,
	[CD_CLIENTE] [int] NOT NULL,
	[NOME] [varchar](100) NOT NULL,
	[NOME_REPRESENTANTE] [varchar](100) NOT NULL,
	[DS_TIPO_CLIENTE] [varchar](3) NOT NULL,
	[DS_EMAIL] [varchar](255) NOT NULL,
	[GRUPO_CLIENTE] [varchar](3) NOT NULL,
	[NUM_TELEFONE_RES] [int] NOT NULL,
	[NUM_TELEFONE_CELL] [int] NOT NULL,
	[NUM_CPF_CNPJ] [varchar](14) NOT NULL,
	[FLG_CNPJ] [varchar](1) NOT NULL,
	[DT_CADASTRO] [date] NOT NULL,
	[DT_NASCIMENTO] [date] NOT NULL,
	[DS_GENERO] [varchar](10) NOT NULL,
	[DS_ESTADO_CIVIL] [varchar](10) NOT NULL,
	[NUM_CEP] [varchar](9) NOT NULL,
	[DS_PAIS] [varchar](2) NOT NULL,
	[DS_ESTADO] [varchar](60) NOT NULL,
	[DS_UF] [varchar](2) NOT NULL,
	[DS_CIDADE] [varchar](10) NOT NULL,
	[DS_BAIRRO] [varchar](80) NOT NULL,
	[DS_LOGRADOURO] [varchar](255) NOT NULL,
	[NUM_ENDERECO] [varchar](10) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_CLIENTE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ((-2)) FOR [CD_CLIENTE]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [NOME]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [NOME_REPRESENTANTE]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DS_TIPO_CLIENTE]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DS_EMAIL]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ((-2)) FOR [NUM_TELEFONE_RES]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ((-2)) FOR [NUM_TELEFONE_CELL]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [NUM_CPF_CNPJ]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [FLG_CNPJ]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DT_CADASTRO]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DT_NASCIMENTO]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DS_GENERO]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DS_ESTADO_CIVIL]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [NUM_CEP]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DS_PAIS]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DS_ESTADO]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DS_UF]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DS_CIDADE]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DS_BAIRRO]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [DS_LOGRADOURO]
GO

ALTER TABLE [dw].[DIM_CLIENTE] ADD  DEFAULT ('-2') FOR [NUM_ENDERECO]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'CD_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'NOME'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Representante do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'NOME_REPRESENTANTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao se o cliente é Ativo ou Desativado' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_TIPO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'E-mail do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_EMAIL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cliente é B2B ou B2C' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'GRUPO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Telefone Residencial do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'NUM_TELEFONE_RES'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Telefone Celular do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'NUM_TELEFONE_CELL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Documento do Cliente CPF ou CNPJ' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'NUM_CPF_CNPJ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag se o Cliente possui CNPJ' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'FLG_CNPJ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data de Cadastro do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DT_CADASTRO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data de Nascimento do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DT_NASCIMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do estado civil do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_GENERO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do estado civil do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_ESTADO_CIVIL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cep de cadastro do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'NUM_CEP'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abreviação do país do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_PAIS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Estado do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_ESTADO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UF do Estado do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_UF'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao da Cidade do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_CIDADE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Bairro do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_BAIRRO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Logradouro do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_LOGRADOURO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero do Endereço do Cliente,podendo conter string quando se tratar de apartamento' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'NUM_ENDERECO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*******************************
*[dw].[DIM_CLIENTE_DISTRIBUIDOR]*
*******************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_CLIENTE_DISTRIBUIDOR]    Script Date: 03/12/2019 19:13:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_CLIENTE_DISTRIBUIDOR](
	[SK_CLIENTE_DISTRIBUIDOR] [int] NOT NULL,
	[CD_CLIENTE] [int] NOT NULL,
	[CD_DISTRIBUIDOR] [int] NOT NULL,
	[DS_TIPO_CLIENTE] [varchar](10) NOT NULL,
	[STS_EXCLUSIVO] [varchar](10) NOT NULL,
	[NOME_CLIENTE] [varchar](100) NOT NULL,
	[NUM_CPF_CNPJ] [varchar](14) NOT NULL,
	[DT_CADASTRO_CLIENTE] [date] NOT NULL,
	[NOME_REPRESENTANTE] [varchar](100) NOT NULL,
	[NOME_DISTRIBUIDOR] [varchar](100) NOT NULL,
	[DS_STS_CLIENTE_DISTRIBUIDOR] [varchar](10) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_CLIENTE_DISTRIBUIDOR] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_CLIENTE_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [CD_CLIENTE]
GO

ALTER TABLE [dw].[DIM_CLIENTE_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [CD_DISTRIBUIDOR]
GO

ALTER TABLE [dw].[DIM_CLIENTE_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [DS_TIPO_CLIENTE]
GO

ALTER TABLE [dw].[DIM_CLIENTE_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [STS_EXCLUSIVO]
GO

ALTER TABLE [dw].[DIM_CLIENTE_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [NOME_CLIENTE]
GO

ALTER TABLE [dw].[DIM_CLIENTE_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [NUM_CPF_CNPJ]
GO

ALTER TABLE [dw].[DIM_CLIENTE_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [DT_CADASTRO_CLIENTE]
GO

ALTER TABLE [dw].[DIM_CLIENTE_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [NOME_REPRESENTANTE]
GO

ALTER TABLE [dw].[DIM_CLIENTE_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [NOME_DISTRIBUIDOR]
GO

ALTER TABLE [dw].[DIM_CLIENTE_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [DS_STS_CLIENTE_DISTRIBUIDOR]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE_DSTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE_DSTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'CD_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE_DSTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'CD_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao se o cliente é Ativo ou Desativado' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE_DSTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_TIPO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cliente Exclusivo no Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE_DSTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'STS_EXCLUSIVO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE_DSTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NOME_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Documento do Cliente CPF ou CNPJ' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE_DSTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_CPF_CNPJ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data de Cadastro do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE_DSTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DT_CADASTRO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Representante do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE_DSTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NOME_REPRESENTANTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE_DSTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NOME_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao do Status do Cliente no Distribuidor, exe: Aprovado/Reprovado' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_CLIENTE_DSTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_STS_CLIENTE_DISTRIBUIDOR'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/************************
*[dw].[DIM_DISTRIBUIDOR]*
************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_DISTRIBUIDOR]    Script Date: 03/12/2019 19:30:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_DISTRIBUIDOR](
	[SK_DISTRIBUIDOR] [int] NOT NULL,
	[CD_DISTRIBUIDOR] [int] NOT NULL,
	[NOME_DISTRIBUIDOR] [varchar](100) NOT NULL,
	[DS_DISTRIBUIDOR] [varchar](255) NOT NULL,
	[DS_STATUS_DISTRIBUIDOR] [varchar](100) NOT NULL,
	[NUM_PRIORIDADE] [int] NOT NULL,
	[NUM_CNPJ] [varchar](14) NOT NULL,
	[DS_EMAIL] [varchar](255) NOT NULL,
	[NUM_CEP_INICIO_RANGE] [varchar](9) NOT NULL,
	[NUM_CEP_FIM_RANGE] [varchar](9) NOT NULL,
	[NUM_TELEFONE_COM] [int] NOT NULL,
	[NUM_TELEFONE_COM2] [int] NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_DISTRIBUIDOR] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_DISTRIBUIDOR] ADD  DEFAULT ((-2)) FOR [CD_DISTRIBUIDOR]
GO

ALTER TABLE [dw].[DIM_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [NOME_DISTRIBUIDOR]
GO

ALTER TABLE [dw].[DIM_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [DS_DISTRIBUIDOR]
GO

ALTER TABLE [dw].[DIM_DISTRIBUIDOR] ADD  CONSTRAINT [DF_DIM_DISTRIBUIDOR_DS_STATUS_DISTRIBUIDOR]  DEFAULT ('-2') FOR [DS_STATUS_DISTRIBUIDOR]
GO

ALTER TABLE [dw].[DIM_DISTRIBUIDOR] ADD  CONSTRAINT [DF_DIM_DISTRIBUIDOR_NUM_PRIORIDADE]  DEFAULT ((-2)) FOR [NUM_PRIORIDADE]
GO

ALTER TABLE [dw].[DIM_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [NUM_CNPJ]
GO

ALTER TABLE [dw].[DIM_DISTRIBUIDOR] ADD  DEFAULT ('-2') FOR [DS_EMAIL]
GO

ALTER TABLE [dw].[DIM_DISTRIBUIDOR] ADD  CONSTRAINT [DF_DIM_DISTRIBUIDOR_NUM_CEP_INICIO_RANGE]  DEFAULT ('-2') FOR [NUM_CEP_INICIO_RANGE]
GO

ALTER TABLE [dw].[DIM_DISTRIBUIDOR] ADD  CONSTRAINT [DF_DIM_DISTRIBUIDOR_NUM_CEP_FIM_RANGE]  DEFAULT ('-2') FOR [NUM_CEP_FIM_RANGE]
GO

ALTER TABLE [dw].[DIM_DISTRIBUIDOR] ADD  DEFAULT ((-2)) FOR [NUM_TELEFONE_COM]
GO

ALTER TABLE [dw].[DIM_DISTRIBUIDOR] ADD  DEFAULT ((-2)) FOR [NUM_TELEFONE_COM2]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'CD_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NOME_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao do Status do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_STATUS_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero da prioridade do distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_PRIORIDADE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Documento do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_CNPJ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'E-mail do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_EMAIL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do telefone do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_TELEFONE_COM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número secundário do telefone do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_TELEFONE_COM2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/***************************
*[dw].[DIM_ENDERECO_PEDIDO]*
***************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_ENDERECO_PEDIDO]    Script Date: 22/11/2019 14:44:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_ENDERECO_PEDIDO](
	[SK_ENDERECO] [int] NOT NULL,
	[NUM_CEP_DO_PEDIDO] [varchar](9) NOT NULL,
	[DS_PAIS_DO_PEDIDO] [varchar](2) NOT NULL,
	[DS_ESTADO_DO_PEDIDO] [varchar](100) NOT NULL,
	[DS_UF_DO_PEDIDO] [varchar](2) NOT NULL,
	[DS_CIDADE_DO_PEDIDO] [varchar](100) NOT NULL,
	[DS_BAIRRO_DO_PEDIDO] [varchar](100) NOT NULL,
	[DS_LOGRADOURO_DO_PEDIDO] [varchar](10) NOT NULL,
	[NUM_ENDERECO_PEDIDO] [varchar](10) NOT NULL,
	[NUM_CEP_DA_ENTREGA] [varchar](9) NOT NULL,
	[DS_PAIS_DA_ENTREGA] [varchar](2) NOT NULL,
	[DS_ESTADO_DA_ENTREGA] [varchar](100) NOT NULL,
	[DS_UF_DA_ENTREGA] [varchar](2) NOT NULL,
	[DS_CIDADE_DA_ENTREGA] [varchar](100) NOT NULL,
	[DS_BAIRRO_DA_ENTREGA] [varchar](100) NOT NULL,
	[DS_LOGRADOURO_DA_ENTREGA] [varchar](100) NOT NULL,
	[NUM_ENDERECO_ENTREGA] [varchar](9) NOT NULL,
	[NUM_CEP_DO_FATURAMENTO] [varchar](9) NOT NULL,
	[DS_PAIS_DO_FATURAMENTO] [varchar](9) NOT NULL,
	[DS_ESTADO_DO_FATURAMENTO] [varchar](100) NOT NULL,
	[DS_UF_DO_FATURAMENTO] [char](10) NOT NULL,
	[DS_CIDADE_DO_FATURAMENTO] [char](10) NOT NULL,
	[DS_BAIRRO_DO_FATURAMENTO] [char](10) NOT NULL,
	[DS_LOGRADOURO_DO_FATURAMENTO] [char](10) NOT NULL,
	[NUM_ENDERECODO_FATURAMENTO] [char](10) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_ENDERECO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [NUM_CEP_DO_PEDIDO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_PAIS_DO_PEDIDO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_ESTADO_DO_PEDIDO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_UF_DO_PEDIDO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_CIDADE_DO_PEDIDO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_BAIRRO_DO_PEDIDO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_LOGRADOURO_DO_PEDIDO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [NUM_ENDERECO_PEDIDO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [NUM_CEP_DA_ENTREGA]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_PAIS_DA_ENTREGA]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_ESTADO_DA_ENTREGA]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_UF_DA_ENTREGA]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_CIDADE_DA_ENTREGA]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_BAIRRO_DA_ENTREGA]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_LOGRADOURO_DA_ENTREGA]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [NUM_ENDERECO_ENTREGA]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [NUM_CEP_DO_FATURAMENTO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_PAIS_DO_FATURAMENTO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_ESTADO_DO_FATURAMENTO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_UF_DO_FATURAMENTO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_CIDADE_DO_FATURAMENTO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_BAIRRO_DO_FATURAMENTO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [DS_LOGRADOURO_DO_FATURAMENTO]
GO

ALTER TABLE [dw].[DIM_ENDERECO_PEDIDO] ADD  DEFAULT ('-2') FOR [NUM_ENDERECODO_FATURAMENTO]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'SK_ENDERECO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Cep da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_CEP_DO_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abreviação do País da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_PAIS_DO_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Estado da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_ESTADO_DO_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição UF da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_UF_DO_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da cidade da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_CIDADE_DO_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do bairro da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_BAIRRO_DO_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Logradouro da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_LOGRADOURO_DO_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero do Logradouro da realização do Pedido, podendo conter string quando se tratar de apartamento' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_ENDERECO_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Cep da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_CEP_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abreviação do País da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_PAIS_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Estado da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_ESTADO_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição UF da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_UF_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da cidade da entega do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_CIDADE_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do bairro da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_BAIRRO_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Logradouro da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_LOGRADOURO_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero do Logradouro da entrega do Pedido, podendo conter string quando se tratar de apartamento' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_ENDERECO_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Cep da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_CEP_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abreviação do País da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_PAIS_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Estado da faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_ESTADO_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição UF da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_UF_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da cidade da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_CIDADE_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do bairro da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_BAIRRO_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Logradouro da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_LOGRADOURO_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero do Logradouro da Faturamento do Pedido, podendo conter string quando se tratar de apartamento' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_ENDERECODO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*****************
*[dw].[DIM_FRONT]*
*****************/


USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_FRONT]    Script Date: 03/12/2019 19:43:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_FRONT](
	[SK_FRONT] [int] NOT NULL,
	[CD_FRONT] [int] NOT NULL,
	[DS_FRONT] [varchar](100) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_FRONT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_FRONT] ADD  DEFAULT ('-2') FOR [CD_FRONT]
GO

ALTER TABLE [dw].[DIM_FRONT] ADD  DEFAULT ('-2') FOR [DS_FRONT]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_FRONT', @level2type=N'COLUMN',@level2name=N'SK_FRONT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de Captacao refente ao Front do Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_FRONT', @level2type=N'COLUMN',@level2name=N'CD_FRONT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao do Front' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_FRONT', @level2type=N'COLUMN',@level2name=N'DS_FRONT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_FRONT', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_FRONT', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*****************
**[dw].[DIM_KIT]*
*****************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_KIT]    Script Date: 04/12/2019 12:08:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_KIT](
	[SK_KIT] [int] NOT NULL,
	[KIT_SKU_ID] [varchar](50) NOT NULL,
	[KIT_SKU_NAME] [int] NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_KIT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_KIT] ADD  DEFAULT ((-2)) FOR [KIT_SKU_ID]
GO

ALTER TABLE [dw].[DIM_KIT] ADD  DEFAULT ((-2)) FOR [KIT_SKU_NAME]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo do Kit do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_KIT', @level2type=N'COLUMN',@level2name=N'KIT_SKU_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Kit do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_KIT', @level2type=N'COLUMN',@level2name=N'KIT_SKU_NAME'
GO




----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*****************
**[dw].[DIM_LOJA]*
*****************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_LOJA]    Script Date: 22/11/2019 15:00:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_LOJA](
	[SK_LOJA] [int] NOT NULL,
	[CD_LOJA] [int] NOT NULL,
	[NOME_LOJA] [varchar](35) NOT NULL,
	[CD_FILIAL] [int] NOT NULL,
	[NOME_FILIAL] [varchar](100) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_LOJA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_LOJA] ADD  DEFAULT ((-2)) FOR [CD_LOJA]
GO

ALTER TABLE [dw].[DIM_LOJA] ADD  DEFAULT ('-2') FOR [NOME_LOJA]
GO

ALTER TABLE [dw].[DIM_LOJA] ADD  DEFAULT ((-2)) FOR [CD_FILIAL]
GO

ALTER TABLE [dw].[DIM_LOJA] ADD  DEFAULT ('-2') FOR [NOME_FILIAL]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'CD_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'NOME_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Filial por Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'CD_FILIAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da Filial' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'NOME_FILIAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/************************
**[dw].[DIM_NOTA_FISCAL]*
************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_LOJA]    Script Date: 22/11/2019 15:00:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_LOJA](
	[SK_LOJA] [int] NOT NULL,
	[CD_LOJA] [int] NOT NULL,
	[NOME_LOJA] [varchar](35) NOT NULL,
	[CD_FILIAL] [int] NOT NULL,
	[NOME_FILIAL] [varchar](100) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_LOJA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_LOJA] ADD  DEFAULT ((-2)) FOR [CD_LOJA]
GO

ALTER TABLE [dw].[DIM_LOJA] ADD  DEFAULT ('-2') FOR [NOME_LOJA]
GO

ALTER TABLE [dw].[DIM_LOJA] ADD  DEFAULT ((-2)) FOR [CD_FILIAL]
GO

ALTER TABLE [dw].[DIM_LOJA] ADD  DEFAULT ('-2') FOR [NOME_FILIAL]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'CD_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'NOME_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Filial por Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'CD_FILIAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da Filial' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'NOME_FILIAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_LOJA', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*******************
**[dw].[DIM_PEDIDO]*
*******************/


USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_PEDIDO]    Script Date: 22/11/2019 15:20:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_PEDIDO](
	[SK_PEDIDO] [int] NOT NULL,
	[CD_PEDIDO] [int] NOT NULL,
	[DT_CRIACAO_PEDIDO] [date] NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_PEDIDO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_PEDIDO] ADD  DEFAULT ((-2)) FOR [CD_PEDIDO]
GO

ALTER TABLE [dw].[DIM_PEDIDO] ADD  DEFAULT ('-2') FOR [DT_CRIACAO_PEDIDO]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PEDIDO', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PEDIDO', @level2type=N'COLUMN',@level2name=N'CD_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da criação do Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PEDIDO', @level2type=N'COLUMN',@level2name=N'DT_CRIACAO_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PEDIDO', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PEDIDO', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*******************
*[dw].[DIM_PRODUTO]*
*******************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_PRODUTO]    Script Date: 22/11/2019 15:49:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_PRODUTO](
	[SK_PRODUTO] [int] NOT NULL,
	[CD_PRODUTO] [int] NOT NULL,
	[CD_EAN] [int] NOT NULL,
	[DS_NOME_PRODUTO] [varchar](255) NOT NULL,
	[DS_TITULO_PRODUTO] [varchar](255) NOT NULL,
	[DS_PRODUTO] [varchar](255) NOT NULL,
	[DS_URL_PRODUTO] [varchar](255) NOT NULL,
	[DS_COR] [varchar](50) NOT NULL,
	[DS_TAMANHO] [varchar](10) NOT NULL,
	[DS_CATEGORIA] [varchar](100) NOT NULL,
	[DS_GENERO] [varchar](10) NOT NULL,
	[DS_MARCA] [varchar](50) NOT NULL,
	[DS_FABRICANTE] [varchar](100) NOT NULL,
	[DS_DE_DIVISAO_DE_PRODUTO] [char](100) NOT NULL,
	[DS_FORNECEDOR] [varchar](100) NOT NULL,
	[DS_STATUS_PRODUTO] [varchar](10) NOT NULL,
	[DS_ESTACAO] [varchar](50) NOT NULL,
	[DS_CAMPANHA] [varchar](50) NOT NULL,
	[FLAG_MARKDOWN] [varchar](1) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_PRODUTO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ((-2)) FOR [CD_PRODUTO]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ((-2)) FOR [CD_EAN]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_NOME_PRODUTO]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_TITULO_PRODUTO]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_PRODUTO]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_URL_PRODUTO]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_COR]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_TAMANHO]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_CATEGORIA]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_GENERO]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_MARCA]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_FABRICANTE]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_DE_DIVISAO_DE_PRODUTO]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_FORNECEDOR]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_STATUS_PRODUTO]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_ESTACAO]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [DS_CAMPANHA]
GO

ALTER TABLE [dw].[DIM_PRODUTO] ADD  DEFAULT ('-2') FOR [FLAG_MARKDOWN]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'CD_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Ean' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'CD_EAN'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Nome Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_NOME_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Titulo do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_TITULO_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Url do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_URL_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Cor do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_COR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição  do Tamanho Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_TAMANHO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Categoria do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_CATEGORIA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Genero  do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_GENERO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Marca do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_MARCA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Fabricante do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_FABRICANTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição de Divisão  do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_DE_DIVISAO_DE_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Fornecedor do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_FORNECEDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Status do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_STATUS_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Estação em que o Produto foi comprado' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_ESTACAO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição se o Produto e referente a alguma campanha' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_CAMPANHA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_PRODUTO', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



/**************************
*[dw].[DIM_REGRA_PROMOCAO]*
**************************/


USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_REGRA_PROMOCAO]    Script Date: 26/11/2019 17:23:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_REGRA_PROMOCAO](
	[SK_REGRA_PROMOCAO] [int] NOT NULL,
	[CD_REGRA] [int] NOT NULL,
	[DS_REGRA] [varchar](100) NOT NULL,
	[CD_REGRA_TEMPLATE] [int] NOT NULL,
	[DS_ORIGEM] [varchar](100) NOT NULL,
	[DT_CRIACAO] [datetime] NOT NULL,
	[DT_EXPIRACAO] [datetime] NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_REGRA_PROMOCAO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_REGRA_PROMOCAO] ADD  DEFAULT ((-2)) FOR [CD_REGRA]
GO

ALTER TABLE [dw].[DIM_REGRA_PROMOCAO] ADD  DEFAULT ('-2') FOR [DS_REGRA]
GO

ALTER TABLE [dw].[DIM_REGRA_PROMOCAO] ADD  DEFAULT ((-2)) FOR [CD_REGRA_TEMPLATE]
GO

ALTER TABLE [dw].[DIM_REGRA_PROMOCAO] ADD  DEFAULT ('-2') FOR [DS_ORIGEM]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'SK_REGRA_PROMOCAO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Regra de Promocão' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'CD_REGRA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Regra de Promoção' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'DS_REGRA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Regra de Promocão do Template' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'CD_REGRA_TEMPLATE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Origem da Regra de Promoção' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'DS_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Criacao da Promocao' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'DT_CRIACAO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data que a Promocao expira' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'DT_EXPIRACAO'
GO
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/***************************************
*[dw].[DIM_STATUS_CLIENTE_DISTRIBUIDOR]*
***************************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_STATUS_CLIENTE_DISTRIBUIDOR]    Script Date: 22/11/2019 15:53:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_STATUS_CLIENTE_DISTRIBUIDOR](
	[SK_STATUS_CLIENTE_DISTRIBUIDOR] [int] NOT NULL,
	[CD_STS_CLIENTE_DISTRIBUIDOR] [int] NOT NULL,
	[DESCRICAO_STATUS_CLIENTE_DISTRIBUIDOR] [char](100) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_STATUS_CLIENTE_DISTRIBUIDOR] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_STATUS_CLIENTE_DISTRIBUIDOR] ADD  DEFAULT ((-2)) FOR [DESCRICAO_STATUS_CLIENTE_DISTRIBUIDOR]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_STATUS_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'SK_STATUS_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de Cliente por Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_STATUS_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'CD_STS_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao dos Status  do Rastreio' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_STATUS_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DESCRICAO_STATUS_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_STATUS_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_STATUS_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*************************
*[dw].[DIM_STATUS_PEDIDO]*
*************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_STATUS_PEDIDO]    Script Date: 22/11/2019 15:54:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_STATUS_PEDIDO](
	[SK_STATUS] [int] NOT NULL,
	[CD_STS_PEDIDO] [int] NOT NULL,
	[DES_STATUS] [char](10) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_STATUS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_STATUS_PEDIDO', @level2type=N'COLUMN',@level2name=N'SK_STATUS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo do Status Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_STATUS_PEDIDO', @level2type=N'COLUMN',@level2name=N'CD_STS_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao dos Status  do Rastreio' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_STATUS_PEDIDO', @level2type=N'COLUMN',@level2name=N'DES_STATUS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_STATUS_PEDIDO', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_STATUS_PEDIDO', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/************************
*[dw].[DIM_TIPO_ESTOQUE]*
************************/


USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_TIPO_ESTOQUE]    Script Date: 26/11/2019 17:27:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_TIPO_ESTOQUE](
	[SK_TIPO_ESTOQUE] [int] NOT NULL,
	[CD_TIPO_ESTOQUE] [int] NOT NULL,
	[DS_ESTOQUE] [varchar](50) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_TIPO_ESTOQUE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_TIPO_ESTOQUE] ADD  DEFAULT ((-2)) FOR [CD_TIPO_ESTOQUE]
GO

ALTER TABLE [dw].[DIM_TIPO_ESTOQUE] ADD  DEFAULT ('-2') FOR [DS_ESTOQUE]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da Tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_TIPO_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_TIPO_ESTOQUE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo do Tipo de Estoque' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_TIPO_ESTOQUE', @level2type=N'COLUMN',@level2name=N'CD_TIPO_ESTOQUE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao do Estoque' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_TIPO_ESTOQUE', @level2type=N'COLUMN',@level2name=N'DS_ESTOQUE'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/**************************
*[dw].[DIM_TIPO_PAGAMENTO]*
**************************/


USE [BI_B2B]
GO

/****** Object:  Table [dw].[DIM_TIPO_PAGAMENTO]    Script Date: 22/11/2019 15:58:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[DIM_TIPO_PAGAMENTO](
	[SK_TIPO_PAGAMENTO] [int] NOT NULL,
	[CD_TIPO_PAGAMENTO] [int] NOT NULL,
	[DS_TIPO_PAGAMENTO] [varchar](50) NOT NULL,
	[DS_TIPO_BANDEIRA] [char](50) NOT NULL,
	[DS_TIPO_VOUCHER] [varchar](50) NOT NULL,
	[DT_INS] [datetime] NOT NULL,
	[DT_UPD] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_TIPO_PAGAMENTO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[DIM_TIPO_PAGAMENTO] ADD  DEFAULT ((-2)) FOR [CD_TIPO_PAGAMENTO]
GO

ALTER TABLE [dw].[DIM_TIPO_PAGAMENTO] ADD  DEFAULT ('-2') FOR [DS_TIPO_PAGAMENTO]
GO

ALTER TABLE [dw].[DIM_TIPO_PAGAMENTO] ADD  DEFAULT ('-2') FOR [DS_TIPO_BANDEIRA]
GO

ALTER TABLE [dw].[DIM_TIPO_PAGAMENTO] ADD  DEFAULT ('-2') FOR [DS_TIPO_VOUCHER]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'SK_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código de Tipo do Pagamento' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'CD_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Tipo de Pagamento ' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Tipo de Bandeira' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_BANDEIRA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Voucher Utilizado' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_VOUCHER'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Atualização do Registro' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'DIM_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'DT_UPD'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

