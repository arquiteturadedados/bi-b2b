
/*CRIACAO DAS TABELAS DO DW UNILEVER*
**TABELAS DE DIMENS�O E FATO*********
**DATA:04/12/2019********************
**AUTOR: BRUNO ANDRADE**************/

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/********************
**[dw].[FAT_ESTOQUE]*
********************/



USE [BI_B2B]
GO

/****** Object:  Table [dw].[FAT_ESTOQUE]    Script Date: 26/11/2019 17:42:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[FAT_ESTOQUE](
	[SK_ESTOQUE] [int] NOT NULL,
	[SK_PRODUTO] [int] NOT NULL,
	[SK_LOJA] [int] NOT NULL,
	[SK_DATA] [int] NOT NULL,
	[SK_DISTRIBUIDOR] [int] NOT NULL,
	[SK_TIPO_ESTOQUE] [int] NOT NULL,
	[QTD_ESTOQUE] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_ESTOQUE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[FAT_ESTOQUE]  WITH CHECK ADD FOREIGN KEY([SK_DATA])
REFERENCES [dw].[DIM_TEMPO] ([SK_DATA])
GO

ALTER TABLE [dw].[FAT_ESTOQUE]  WITH CHECK ADD FOREIGN KEY([SK_DISTRIBUIDOR])
REFERENCES [dw].[DIM_DISTRIBUIDOR] ([SK_DISTRIBUIDOR])
GO

ALTER TABLE [dw].[FAT_ESTOQUE]  WITH CHECK ADD FOREIGN KEY([SK_LOJA])
REFERENCES [dw].[DIM_LOJA] ([SK_LOJA])
GO

ALTER TABLE [dw].[FAT_ESTOQUE]  WITH CHECK ADD FOREIGN KEY([SK_PRODUTO])
REFERENCES [dw].[DIM_PRODUTO] ([SK_PRODUTO])
GO

ALTER TABLE [dw].[FAT_ESTOQUE]  WITH CHECK ADD FOREIGN KEY([SK_TIPO_ESTOQUE])
REFERENCES [dw].[DIM_TIPO_ESTOQUE] ([SK_TIPO_ESTOQUE])
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da Tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_ESTOQUE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave primaria da tabela de Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave primaria da tabela de Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave primaria da tabela de Data' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave primaria da tabela de Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave primaria da tabela de Tipo de Estoque' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'SK_TIPO_ESTOQUE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade do Estoque' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_ESTOQUE', @level2type=N'COLUMN',@level2name=N'QTD_ESTOQUE'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*****************************
*[dw].[FAT_LIMITE_DE_CREDITO]*
****************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[FAT_LIMITE_DE_CREDITO]    Script Date: 04/12/2019 11:10:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[FAT_LIMITE_DE_CREDITO](
	[SK_LIMITE_DE_CREDITO] [int] NOT NULL,
	[SK_CLIENTE] [int] NOT NULL,
	[SK_DISTRIBUIDOR] [int] NOT NULL,
	[SK_CLIENTE_DISTRIBUIDOR] [int] NOT NULL,
	[SK_DATA] [int] NOT NULL,
	[SK_LOJA] [int] NOT NULL,
	[SK_STATUS_CLIENTE_DISTRIBUIDOR] [int] NOT NULL,
	[VLR_LIMITE_DE_CREDITO] [numeric](10, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_LIMITE_DE_CREDITO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[FAT_LIMITE_DE_CREDITO]  WITH CHECK ADD FOREIGN KEY([SK_CLIENTE])
REFERENCES [dw].[DIM_CLIENTE] ([SK_CLIENTE])
GO

ALTER TABLE [dw].[FAT_LIMITE_DE_CREDITO]  WITH CHECK ADD FOREIGN KEY([SK_DATA])
REFERENCES [dw].[DIM_TEMPO] ([SK_DATA])
GO

ALTER TABLE [dw].[FAT_LIMITE_DE_CREDITO]  WITH CHECK ADD FOREIGN KEY([SK_DISTRIBUIDOR])
REFERENCES [dw].[DIM_DISTRIBUIDOR] ([SK_DISTRIBUIDOR])
GO

ALTER TABLE [dw].[FAT_LIMITE_DE_CREDITO]  WITH CHECK ADD FOREIGN KEY([SK_LOJA])
REFERENCES [dw].[DIM_LOJA] ([SK_LOJA])
GO

ALTER TABLE [dw].[FAT_LIMITE_DE_CREDITO]  WITH CHECK ADD FOREIGN KEY([SK_STATUS_CLIENTE_DISTRIBUIDOR])
REFERENCES [dw].[DIM_STATUS_CLIENTE_DISTRIBUIDOR] ([SK_STATUS_CLIENTE_DISTRIBUIDOR])
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_LIMITE_DE_CREDITO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Status Cliente Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'SK_STATUS_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do limite de Credito , Cliente por Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_LIMITE_DE_CREDITO', @level2type=N'COLUMN',@level2name=N'VLR_LIMITE_DE_CREDITO'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*********************
*[dw].[FAT_PAGAMENTO]*
*********************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[FAT_PAGAMENTO]    Script Date: 04/12/2019 11:21:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[FAT_PAGAMENTO](
	[SK_PAGAMENTO] [int] NOT NULL,
	[SK_PEDIDO] [int] NOT NULL,
	[SK_TIPO_PAGAMENTO] [int] NOT NULL,
	[SK_LOJA] [int] NOT NULL,
	[SK_DATA] [int] NOT NULL,
	[VALOR_POR_TIPO_PAGAMENTO] [numeric](10, 2)NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_PAGAMENTO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[FAT_PAGAMENTO] ADD  DEFAULT ((-2)) FOR [SK_PEDIDO]
GO

ALTER TABLE [dw].[FAT_PAGAMENTO] ADD  DEFAULT ((-2)) FOR [SK_TIPO_PAGAMENTO]
GO

ALTER TABLE [dw].[FAT_PAGAMENTO] ADD  DEFAULT ((-2)) FOR [SK_LOJA]
GO

ALTER TABLE [dw].[FAT_PAGAMENTO] ADD  DEFAULT ((-2)) FOR [SK_DATA]
GO

ALTER TABLE [dw].[FAT_PAGAMENTO]  WITH CHECK ADD FOREIGN KEY([SK_DATA])
REFERENCES [dw].[DIM_TEMPO] ([SK_DATA])
GO

ALTER TABLE [dw].[FAT_PAGAMENTO]  WITH CHECK ADD FOREIGN KEY([SK_LOJA])
REFERENCES [dw].[DIM_LOJA] ([SK_LOJA])
GO

ALTER TABLE [dw].[FAT_PAGAMENTO]  WITH CHECK ADD FOREIGN KEY([SK_PEDIDO])
REFERENCES [dw].[DIM_PEDIDO] ([SK_PEDIDO])
GO

ALTER TABLE [dw].[FAT_PAGAMENTO]  WITH CHECK ADD FOREIGN KEY([SK_TIPO_PAGAMENTO])
REFERENCES [dw].[DIM_TIPO_PAGAMENTO] ([SK_TIPO_PAGAMENTO])
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'SK_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tipo Pagamento' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'SK_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Pagor por Tipo de pagamento no pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'VALOR_POR_TIPO_PAGAMENTO'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*******************************
*[dw].[FAT_PEDIDO_CAPTADO_ITEM]*
*******************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[FAT_PEDIDO_CAPTADO_ITEM]    Script Date: 04/12/2019 12:16:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM](
	[SK_PEDIDO_CAPTADO_ITEM] [int] NOT NULL,
	[SK_PEDIDO] [int] NOT NULL,
	[SK_PRODUTO] [int] NOT NULL,
	[SK_CLIENTE] [int] NOT NULL,
	[SK_DATA] [int] NOT NULL,
	[SK_LOJA] [int] NOT NULL,
	[SK_KIT] [int] NOT NULL,
	[SK_ENDERECO] [int] NOT NULL,
	[SK_DISTRIBUIDOR] [int] NOT NULL,
	[SK_CANAL_VENDA] [int] NOT NULL,
	[SK_TIPO_PAGAMENTO] [int] NOT NULL,
	[SK_CLIENTE_DISTRIBUIDOR] [int] NOT NULL,
	[SK_FRONT] [int] NOT NULL,
	[QTD_ITEM] [int] NOT NULL,
	[VLR_ITEM] [numeric](10, 2) NOT NULL,
	[VLR_DESCONTO] [numeric](10, 2) NOT NULL,
	[VLR_PEDIDO_REAL] [numeric](10, 2) NOT NULL,
	[VLR_MIDDLEWARE] [numeric](10, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_PEDIDO_CAPTADO_ITEM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_CANAL_VENDA])
REFERENCES [dw].[DIM_CANAL_VENDA] ([SK_CANAL_VENDA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_CLIENTE])
REFERENCES [dw].[DIM_CLIENTE] ([SK_CLIENTE])
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_CLIENTE_DISTRIBUIDOR])
REFERENCES [dw].[DIM_CLIENTE_DSTRIBUIDOR] ([SK_CLIENTE_DISTRIBUIDOR])
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_DATA])
REFERENCES [dw].[DIM_TEMPO] ([SK_DATA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_DISTRIBUIDOR])
REFERENCES [dw].[DIM_DISTRIBUIDOR] ([SK_DISTRIBUIDOR])
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_ENDERECO])
REFERENCES [dw].[DIM_ENDERECO_PEDIDO] ([SK_ENDERECO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_FRONT])
REFERENCES [dw].[DIM_FRONT] ([SK_FRONT])
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_KIT])
REFERENCES [dw].[DIM_KIT] ([SK_KIT])
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_LOJA])
REFERENCES [dw].[DIM_LOJA] ([SK_LOJA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_PEDIDO])
REFERENCES [dw].[DIM_PEDIDO] ([SK_PEDIDO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_PRODUTO])
REFERENCES [dw].[DIM_PRODUTO] ([SK_PRODUTO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_CAPTADO_ITEM]  WITH CHECK ADD FOREIGN KEY([SK_TIPO_PAGAMENTO])
REFERENCES [dw].[DIM_TIPO_PAGAMENTO] ([SK_TIPO_PAGAMENTO])
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO_CAPTADO_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Kit' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_KIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Endere�o' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_ENDERECO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Canal de Venda' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_CANAL_VENDA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tipo de Pagamento' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Front' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'SK_FRONT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade do item por Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'QTD_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Item por Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'VLR_ITEM_TABELA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do desconto do Pedido por item' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'VLR_DESCONTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Real do Pedido por Item' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'VLR_PEDIDO_REAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Middleware' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_CAPTADO_ITEM', @level2type=N'COLUMN',@level2name=N'VLR_MIDDLEWARE'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/**************************
*[dw].[FAT_PEDIDO_DEV_EXT]*
**************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[FAT_PEDIDO_DEV_EXT]    Script Date: 04/12/2019 12:39:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[FAT_PEDIDO_DEV_EXT](
	[SK_PEDIDO_DEV_EXT] [int] NOT NULL,
	[SK_PRODUTO] [int] NOT NULL,
	[SK_KIT] [int] NOT NULL,
	[SK_CLIENTE_DISTRIBUIDOR] [int] NOT NULL,
	[SK_FRONT] [int] NOT NULL,
	[SK_DATA] [int] NOT NULL,
	[SK_CANAL_VENDA] [int] NOT NULL,
	[SK_ENDERECO] [int] NOT NULL,
	[SK_PEDIDO] [int] NOT NULL,
	[SK_CLIENTE] [int] NOT NULL,
	[SK_LOJA] [int] NOT NULL,
	[SK_DISTRIBUIDOR] [int] NOT NULL,
	[SK_TIPO_PAGAMENTO] [int] NOT NULL,
	[QTD_ITEM] [int] NOT NULL,
	[VLR_ITEM] [numeric](10, 2) NOT NULL,
	[VLR_DESCONTO] [numeric](10, 2) NOT NULL,
	[VLR_PEDIDO_REAL] [numeric](10, 2) NOT NULL,
	[VLR_TOTAL_NF] [numeric](10, 2) NOT NULL,
	[VLR_TOTAL_PEDIDO] [numeric](10, 2) NOT NULL,
	[VLR_ICMS_ST] [numeric](10, 2) NOT NULL,
	[VLR_ICMS] [numeric](10, 2) NOT NULL,
	[VLR_PIS] [numeric](10, 2) NOT NULL,
	[VLR_COFINS] [numeric](10, 2) NOT NULL,

PRIMARY KEY CLUSTERED 
(
	[SK_PEDIDO_DEV_EXT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_CANAL_VENDA])
REFERENCES [dw].[DIM_CANAL_VENDA] ([SK_CANAL_VENDA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_CLIENTE])
REFERENCES [dw].[DIM_CLIENTE] ([SK_CLIENTE])
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_CLIENTE_DISTRIBUIDOR])
REFERENCES [dw].[DIM_CLIENTE_DSTRIBUIDOR] ([SK_CLIENTE_DISTRIBUIDOR])
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_DATA])
REFERENCES [dw].[DIM_TEMPO] ([SK_DATA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_DISTRIBUIDOR])
REFERENCES [dw].[DIM_DISTRIBUIDOR] ([SK_DISTRIBUIDOR])
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_ENDERECO])
REFERENCES [dw].[DIM_ENDERECO_PEDIDO] ([SK_ENDERECO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_FRONT])
REFERENCES [dw].[DIM_FRONT] ([SK_FRONT])
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_KIT])
REFERENCES [dw].[DIM_KIT] ([SK_KIT])
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_LOJA])
REFERENCES [dw].[DIM_LOJA] ([SK_LOJA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_PEDIDO])
REFERENCES [dw].[DIM_PEDIDO] ([SK_PEDIDO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_PRODUTO])
REFERENCES [dw].[DIM_PRODUTO] ([SK_PRODUTO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_DEV_EXT]  WITH CHECK ADD FOREIGN KEY([SK_TIPO_PAGAMENTO])
REFERENCES [dw].[DIM_TIPO_PAGAMENTO] ([SK_TIPO_PAGAMENTO])
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO_DEV_EXT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Kit' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_KIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Canal Venda' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_CANAL_VENDA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Endere�o' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_ENDERECO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tipo Pagamento' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de Itens  Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'QTD_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Desconto por Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_DESCONTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Real do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_PEDIDO_REAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Total da Nota Fiscal do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_TOTAL_NF'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Total do Pedido do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_TOTAL_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor de ICMS_ST do  Pedido do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_ICMS_ST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor de ICMS_ST do  Pedido do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_ICMS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do PIS do Pedido do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_PIS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Cofins do Pedido do Item Devolvido ou extraviado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'VLR_COFINS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Front' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_DEV_EXT', @level2type=N'COLUMN',@level2name=N'SK_FRONT'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE [BI_B2B]
GO

/****** Object:  Table [dw].[FAT_PEDIDO_FATURADO]    Script Date: 04/12/2019 13:08:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[FAT_PEDIDO_FATURADO](
	[SK_PEDIDO_FATURADO] [int] NOT NULL,
	[SK_PEDIDO] [int] NOT NULL,
	[SK_NOTA_FISCAL] [int] NOT NULL,
	[SK_PRODUTO] [int] NOT NULL,
	[SK_PROMOCAO] [int] NOT NULL,
	[SK_DATA] [int] NOT NULL,
	[SK_CANAL_DE_VENDA] [int] NOT NULL,
	[SK_ENDERECO] [int] NOT NULL,
	[SK_CLIENTE] [int] NOT NULL,
	[SK_LOJA] [int] NOT NULL,
	[SK_DISTRIBUIDOR] [int] NOT NULL,
	[SK_TIPO_PAGAMENTO] [int] NOT NULL,
	[QTD_ITEM] [int] NOT NULL,
	[VLR_ITEM] [numeric](10, 2) NOT NULL,
	[VLR_DESCONTO] [numeric](10, 2) NOT NULL,
	[VLR_PEDIDO_REAL] [numeric](10, 2) NOT NULL,
	[VLR_TOTAL_NF] [numeric](10, 2) NOT NULL,
	[VLR_TOTAL_PEDIDO] [numeric](10, 2) NOT NULL,
	[VLR_ICMS_ST] [numeric](10, 2) NOT NULL,
	[VLR_ICMS] [numeric](10, 2) NOT NULL,
	[VLR_PIS] [numeric](10, 2) NOT NULL,
	[VLR_COFINS] [numeric](10, 2) NOT NULL,
	[FATOR_QTD] [numeric](10, 4) NOT NULL,
	[QTD_BONIFICADA] [int] NOT NULL,
	[QTD_BONIFICADA_FAT] [int] NOT NULL,
	[PCT_BONIFICADA_FAT] [int] NOT NULL,
	[QTD_CAVALO] [int] NOT NULL,
	[QTD_CAVALO_FAT] [int] NOT NULL,
	[PCT_CAVALO_FAT] [int] NOT NULL,
	[FATOR_BONIFICADO] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_PEDIDO_FATURADO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_CANAL_DE_VENDA])
REFERENCES [dw].[DIM_CANAL_VENDA] ([SK_CANAL_VENDA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_CANAL_DE_VENDA])
REFERENCES [dw].[DIM_CANAL_VENDA] ([SK_CANAL_VENDA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_CLIENTE])
REFERENCES [dw].[DIM_CLIENTE] ([SK_CLIENTE])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_CLIENTE])
REFERENCES [dw].[DIM_CLIENTE] ([SK_CLIENTE])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_DATA])
REFERENCES [dw].[DIM_TEMPO] ([SK_DATA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_DATA])
REFERENCES [dw].[DIM_TEMPO] ([SK_DATA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_DISTRIBUIDOR])
REFERENCES [dw].[DIM_DISTRIBUIDOR] ([SK_DISTRIBUIDOR])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_DISTRIBUIDOR])
REFERENCES [dw].[DIM_DISTRIBUIDOR] ([SK_DISTRIBUIDOR])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_ENDERECO])
REFERENCES [dw].[DIM_ENDERECO_PEDIDO] ([SK_ENDERECO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_ENDERECO])
REFERENCES [dw].[DIM_ENDERECO_PEDIDO] ([SK_ENDERECO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_LOJA])
REFERENCES [dw].[DIM_LOJA] ([SK_LOJA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_LOJA])
REFERENCES [dw].[DIM_LOJA] ([SK_LOJA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_NOTA_FISCAL])
REFERENCES [dw].[DIM_NOTA_FISCAL] ([SK_NOTA_FISCAL])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_NOTA_FISCAL])
REFERENCES [dw].[DIM_NOTA_FISCAL] ([SK_NOTA_FISCAL])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_PEDIDO])
REFERENCES [dw].[DIM_PEDIDO] ([SK_PEDIDO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_PEDIDO])
REFERENCES [dw].[DIM_PEDIDO] ([SK_PEDIDO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_PRODUTO])
REFERENCES [dw].[DIM_PRODUTO] ([SK_PRODUTO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_PRODUTO])
REFERENCES [dw].[DIM_PRODUTO] ([SK_PRODUTO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_TIPO_PAGAMENTO])
REFERENCES [dw].[DIM_TIPO_PAGAMENTO] ([SK_TIPO_PAGAMENTO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_FATURADO]  WITH CHECK ADD FOREIGN KEY([SK_TIPO_PAGAMENTO])
REFERENCES [dw].[DIM_TIPO_PAGAMENTO] ([SK_TIPO_PAGAMENTO])
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO_FATURADO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Nota Fiscal' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_NOTA_FISCAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Promo��o' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_PROMOCAO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Canal Venda' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_CANAL_DE_VENDA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela  Endereco' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_ENDERECO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Cliente' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tipo Pagamento' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'SK_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de item Faturado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'QTD_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor de item Faturado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Desconto Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_DESCONTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Real  Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_PEDIDO_REAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Total da Nota Fiscal  Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_TOTAL_NF'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Total do Pedido do Item  Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_TOTAL_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Total do Pedido do Item  Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_ICMS_ST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor de ICMS do Item Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_ICMS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor de ICMS do Item Faturado  por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_PIS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do Cofins do Item Faturado por Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'VLR_COFINS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fator de pondera��o da verba aplicada aos produtos n�o bonificado' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'FATOR_QTD'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'� a mesma informa��o da coluna da tabela ifc TBBR_ORDER_LINE_PROMOTION_BI, por�m em outra granularidade e na tabela ECAD_ORDER_LINE' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'QTD_BONIFICADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de unidades bonificadas que foram faturadas' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'QTD_BONIFICADA_FAT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Percentual das unidades bonificadas que foram faturadas' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'PCT_BONIFICADA_FAT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de unidades que s�o cavalos' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'QTD_CAVALO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de unidades cavalos que foram faturadas' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'QTD_CAVALO_FAT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Percentual dos Cavalos que foi faturado' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'PCT_CAVALO_FAT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fator de pondera��o da verba aplicada aos produtos bonificados' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_FATURADO', @level2type=N'COLUMN',@level2name=N'FATOR_BONIFICADO'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/**********************************
*[dw].[FAT_PEDIDO_STATUS_RASTREIO]*
**********************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[FAT_PEDIDO_STATUS_RASTREIO]    Script Date: 22/11/2019 18:33:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[FAT_PEDIDO_STATUS_RASTREIO](
	[SK_PEDIDO_STATUS_RASTREIO] [int] NOT NULL,
	[SK_DATA] [int] NOT NULL,
	[SK_STATUS] [int] NOT NULL,
	[SK_LOJA] [int] NOT NULL,
	[SK_DISTRIBUIDOR] [int] NOT NULL,
	[SK_PEDIDO] [int] NOT NULL,
	[SK_NOTA_FISCAL] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_PEDIDO_STATUS_RASTREIO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[FAT_PEDIDO_STATUS_RASTREIO]  WITH CHECK ADD FOREIGN KEY([SK_DATA])
REFERENCES [dw].[DIM_TEMPO] ([SK_DATA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_STATUS_RASTREIO]  WITH CHECK ADD FOREIGN KEY([SK_DISTRIBUIDOR])
REFERENCES [dw].[DIM_DISTRIBUIDOR] ([SK_DISTRIBUIDOR])
GO

ALTER TABLE [dw].[FAT_PEDIDO_STATUS_RASTREIO]  WITH CHECK ADD FOREIGN KEY([SK_LOJA])
REFERENCES [dw].[DIM_LOJA] ([SK_LOJA])
GO

ALTER TABLE [dw].[FAT_PEDIDO_STATUS_RASTREIO]  WITH CHECK ADD FOREIGN KEY([SK_NOTA_FISCAL])
REFERENCES [dw].[DIM_NOTA_FISCAL] ([SK_NOTA_FISCAL])
GO

ALTER TABLE [dw].[FAT_PEDIDO_STATUS_RASTREIO]  WITH CHECK ADD FOREIGN KEY([SK_PEDIDO])
REFERENCES [dw].[DIM_PEDIDO] ([SK_PEDIDO])
GO

ALTER TABLE [dw].[FAT_PEDIDO_STATUS_RASTREIO]  WITH CHECK ADD FOREIGN KEY([SK_STATUS])
REFERENCES [dw].[DIM_STATUS_PEDIDO] ([SK_STATUS])
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tempo' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Status' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_STATUS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Nota Fiscal' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PEDIDO_STATUS_RASTREIO', @level2type=N'COLUMN',@level2name=N'SK_NOTA_FISCAL'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*****************************
*[dw].[FAT_PRODUTO_HISTORICO]*
*****************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[FAT_PRODUTO_HISTORICO]    Script Date: 26/11/2019 18:05:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[FAT_PRODUTO_HISTORICO](
	[SK_PRODUTO_HISTORICO] [int] NOT NULL,
	[SK_PRODUTO] [int] NOT NULL,
	[SK_DISTRIBUIDOR] [int] NOT NULL,
	[SK_DATA] [int] NOT NULL,
	[SK_LOJA] [int] NOT NULL,
	[PRECO_DE] [numeric](10, 2) NOT NULL,
	[PRECO_POR] [numeric](10, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_PRODUTO_HISTORICO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[FAT_PRODUTO_HISTORICO]  WITH CHECK ADD FOREIGN KEY([SK_DATA])
REFERENCES [dw].[DIM_TEMPO] ([SK_DATA])
GO

ALTER TABLE [dw].[FAT_PRODUTO_HISTORICO]  WITH CHECK ADD FOREIGN KEY([SK_DISTRIBUIDOR])
REFERENCES [dw].[DIM_DISTRIBUIDOR] ([SK_DISTRIBUIDOR])
GO

ALTER TABLE [dw].[FAT_PRODUTO_HISTORICO]  WITH CHECK ADD FOREIGN KEY([SK_LOJA])
REFERENCES [dw].[DIM_LOJA] ([SK_LOJA])
GO

ALTER TABLE [dw].[FAT_PRODUTO_HISTORICO]  WITH CHECK ADD FOREIGN KEY([SK_PRODUTO])
REFERENCES [dw].[DIM_PRODUTO] ([SK_PRODUTO])
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO_HISTORICO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Distribuidor' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'SK_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Preco De do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'PRECO_DE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Preco Por do Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PRODUTO_HISTORICO', @level2type=N'COLUMN',@level2name=N'PRECO_POR'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*****************************
*[dw].[FAT_PROMOCAO_APLICADA]*
*****************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[FAT_PROMOCAO_APLICADA]    Script Date: 04/12/2019 13:16:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[FAT_PROMOCAO_APLICADA](
	[SK_PROMOCAO_APLICADA] [int] NOT NULL,
	[SK_PEDIDO] [int] NOT NULL,
	[SK_PRODUTO] [int] NOT NULL,
	[SK_REGRA_PROMOCAO] [int] NOT NULL,
	[SK_DATA] [int] NOT NULL,
	[SK_KIT] [int] NOT NULL,
	[SK_LOJA] [int] NOT NULL,
	[VLR_PROMO_ITEM] [numeric](10, 2) NOT NULL,
	[QTD_PROMO_APLICADA] [int] NOT NULL,
	[QTD_MIN_PROMO] [int] NOT NULL,
	[QTD_ITEM_TOTAL] [int] NOT NULL,
	[VLR_PROMO_APLICADA] [numeric](10, 2) NOT NULL,
	[QTD_BONIFICADA] [int] NOT NULL,
	[PROMO_BONIFICADA] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_PROMOCAO_APLICADA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[FAT_PROMOCAO_APLICADA]  WITH CHECK ADD FOREIGN KEY([SK_DATA])
REFERENCES [dw].[DIM_TEMPO] ([SK_DATA])
GO

ALTER TABLE [dw].[FAT_PROMOCAO_APLICADA]  WITH CHECK ADD FOREIGN KEY([SK_KIT])
REFERENCES [dw].[DIM_KIT] ([SK_KIT])
GO

ALTER TABLE [dw].[FAT_PROMOCAO_APLICADA]  WITH CHECK ADD FOREIGN KEY([SK_LOJA])
REFERENCES [dw].[DIM_LOJA] ([SK_LOJA])
GO

ALTER TABLE [dw].[FAT_PROMOCAO_APLICADA]  WITH CHECK ADD FOREIGN KEY([SK_PEDIDO])
REFERENCES [dw].[DIM_PEDIDO] ([SK_PEDIDO])
GO

ALTER TABLE [dw].[FAT_PROMOCAO_APLICADA]  WITH CHECK ADD FOREIGN KEY([SK_PRODUTO])
REFERENCES [dw].[DIM_PRODUTO] ([SK_PRODUTO])
GO

ALTER TABLE [dw].[FAT_PROMOCAO_APLICADA]  WITH CHECK ADD FOREIGN KEY([SK_REGRA_PROMOCAO])
REFERENCES [dw].[DIM_REGRA_PROMOCAO] ([SK_REGRA_PROMOCAO])
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_PROMOCAO_APLICADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Regra de Promocao' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_REGRA_PROMOCAO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Kit' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_KIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valoda promo��o do item' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'VLR_PROMO_ITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de itens com a promocao aplicada' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'QTD_PROMO_APLICADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade minima de itens para a promocao' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'QTD_MIN_PROMO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade total de itens' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'QTD_ITEM_TOTAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor da Promocao aplicada' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'VLR_PROMO_APLICADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade bonificada' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'QTD_BONIFICADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Promocao Bonificada' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_APLICADA', @level2type=N'COLUMN',@level2name=N'PROMO_BONIFICADA'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*****************************
*[dw].[FAT_PROMOCAO_FATURADA]*
*****************************/

USE [BI_B2B]
GO

/****** Object:  Table [dw].[FAT_PROMOCAO_FATURADA]    Script Date: 04/12/2019 13:23:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dw].[FAT_PROMOCAO_FATURADA](
	[SK_PROMOCAO_FATURADA] [int] NOT NULL,
	[SK_PEDIDO] [int] NOT NULL,
	[SK_KIT] [int] NOT NULL,
	[SK_PRODUTO] [int] NOT NULL,
	[SK_LOJA] [int] NOT NULL,
	[SK_TIPO_PAGAMENTO] [int] NOT NULL,
	[SK_DATA] [int] NOT NULL,
	[VALOR_ITEM_PROMO_FATURADO] Numeric (10,2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SK_PROMOCAO_FATURADA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dw].[FAT_PROMOCAO_FATURADA]  WITH CHECK ADD FOREIGN KEY([SK_DATA])
REFERENCES [dw].[DIM_TEMPO] ([SK_DATA])
GO

ALTER TABLE [dw].[FAT_PROMOCAO_FATURADA]  WITH CHECK ADD FOREIGN KEY([SK_KIT])
REFERENCES [dw].[DIM_KIT] ([SK_KIT])
GO

ALTER TABLE [dw].[FAT_PROMOCAO_FATURADA]  WITH CHECK ADD FOREIGN KEY([SK_LOJA])
REFERENCES [dw].[DIM_LOJA] ([SK_LOJA])
GO

ALTER TABLE [dw].[FAT_PROMOCAO_FATURADA]  WITH CHECK ADD FOREIGN KEY([SK_PEDIDO])
REFERENCES [dw].[DIM_PEDIDO] ([SK_PEDIDO])
GO

ALTER TABLE [dw].[FAT_PROMOCAO_FATURADA]  WITH CHECK ADD FOREIGN KEY([SK_PRODUTO])
REFERENCES [dw].[DIM_PRODUTO] ([SK_PRODUTO])
GO

ALTER TABLE [dw].[FAT_PROMOCAO_FATURADA]  WITH CHECK ADD FOREIGN KEY([SK_TIPO_PAGAMENTO])
REFERENCES [dw].[DIM_TIPO_PAGAMENTO] ([SK_TIPO_PAGAMENTO])
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_PROMOCAO_FATURADA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Pedido' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela KIT' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_KIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Produto' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Loja' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Tipo Pagamento' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Surrogate Key da tabela Data' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'SK_DATA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor Faturado da Promocao por Pedido item' , @level0type=N'SCHEMA',@level0name=N'dw', @level1type=N'TABLE',@level1name=N'FAT_PROMOCAO_FATURADA', @level2type=N'COLUMN',@level2name=N'VALOR_ITEM_PROMO_FATURADO'
GO


