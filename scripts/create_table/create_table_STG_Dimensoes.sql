
/*CRIACAO DAS TABELAS DO DW UNILEVER*
**TABELAS DE DIMENSÃO E FATO*********
**DATA:04/12/2019********************
**AUTOR: BRUNO ANDRADE**************/



/***********************
*BI_B2B.[STG_CANAL_VENDA]*
***********************/

USE [ADD_STAGING]
GO

/****** Object:  Table [bi_b2b].[STG_CANAL_VENDA]    Script Date: 12/12/2019 17:18:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [bi_b2b].[STG_CANAL_VENDA](
	[CD_CANAL_VENDA] [int] NOT NULL,
	[DS_CANAL_VENDA] [varchar](8000) NULL,
	[DS_TIPO_ORIGEM] [varchar](8000) NULL,
	[DT_INS] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CD_CANAL_VENDA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo do Canal de Venda' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CANAL_VENDA', @level2type=N'COLUMN',@level2name=N'CD_CANAL_VENDA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao Canal De Venda' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CANAL_VENDA', @level2type=N'COLUMN',@level2name=N'DS_CANAL_VENDA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CANAL_VENDA', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CANAL_VENDA', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*******************
*BI_B2B.[STG_CLIENTE]*
*******************/

USE [ADD_STAGING]
GO

/****** Object:  Table [bi_b2b].[STG_CLIENTE]    Script Date: 12/12/2019 17:19:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [bi_b2b].[STG_CLIENTE](
	[CD_CLIENTE] [int] NOT NULL,
	[NOME] [varchar](8000) NULL,
	[NOME_REPRESENTANTE] [varchar](8000) NULL,
	[DS_TIPO_CLIENTE] [varchar](8000) NULL,
	[DS_EMAIL] [varchar](8000) NULL,
	[GRUPO_CLIENTE] [varchar](8000) NULL,
	[NUM_TELEFONE_RES] [int] NULL,
	[NUM_TELEFONE_CELL] [int] NULL,
	[NUM_CPF_CNPJ] [varchar](8000) NULL,
	[FLG_CNPJ] [varchar](8000) NULL,
	[DT_CADASTRO] [date] NULL,
	[DT_NASCIMENTO] [date] NULL,
	[DS_GENERO] [varchar](8000) NULL,
	[DS_ESTADO_CIVIL] [varchar](8000) NULL,
	[NUM_CEP] [varchar](8000) NULL,
	[DS_PAIS] [varchar](8000) NULL,
	[DS_ESTADO] [varchar](8000) NULL,
	[DS_UF] [varchar](8000) NULL,
	[DS_CIDADE] [varchar](8000) NULL,
	[DS_BAIRRO] [varchar](8000) NULL,
	[DS_LOGRADOURO] [varchar](8000) NULL,
	[NUM_ENDERECO] [varchar](8000) NULL,
	[DS_TIPO_ORIGEM] [varchar](8000) NULL,
	[DT_INS] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CD_CLIENTE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'CD_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'NOME'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Representante do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'NOME_REPRESENTANTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao se o cliente é Ativo ou Desativado' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_TIPO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'E-mail do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_EMAIL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cliente é B2B ou B2C' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'GRUPO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Telefone Residencial do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'NUM_TELEFONE_RES'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Telefone Celular do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'NUM_TELEFONE_CELL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Documento do Cliente CPF ou CNPJ' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'NUM_CPF_CNPJ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag se o Cliente possui CNPJ' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'FLG_CNPJ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data de Cadastro do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DT_CADASTRO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data de Nascimento do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DT_NASCIMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do estado civil do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_GENERO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do estado civil do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_ESTADO_CIVIL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cep de cadastro do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'NUM_CEP'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abreviação do país do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_PAIS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Estado do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_ESTADO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UF do Estado do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_UF'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao da Cidade do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_CIDADE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Bairro do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_BAIRRO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Logradouro do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_LOGRADOURO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero do Endereço do Cliente,podendo conter string quando se tratar de apartamento' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'NUM_ENDERECO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO




----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*******************************
*BI_B2B.[STG_CLIENTE_DISTRIBUIDOR]*
*******************************/

USE [ADD_STAGING]
GO

/****** Object:  Table [bi_b2b].[STG_CLIENTE_DISTRIBUIDOR]    Script Date: 12/12/2019 17:36:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [bi_b2b].[STG_CLIENTE_DISTRIBUIDOR](
	[CD_CLIENTE] [int] NOT NULL,
	[CD_DISTRIBUIDOR] [int] NULL,
	[DS_TIPO_CLIENTE] [varchar](8000) NULL,
	[STS_EXCLUSIVO] [varchar](8000) NULL,
	[NOME_CLIENTE] [varchar](8000) NULL,
	[NUM_CPF_CNPJ] [varchar](8000) NULL,
	[DT_CADASTRO_CLIENTE] [date] NULL,
	[NOME_REPRESENTANTE] [varchar](8000) NULL,
	[NOME_DISTRIBUIDOR] [varchar](8000) NULL,
	[DS_STATUS_CLIENTE_DISTRIBUIDOR] [varchar](8000) NULL,
	[DS_TIPO_ORIGEM] [varchar](8000) NULL,
	[DT_INS] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CD_CLIENTE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave Primaria da tabela' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'CD_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'CD_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao se o cliente é Ativo ou Desativado' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_TIPO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cliente Exclusivo no Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'STS_EXCLUSIVO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NOME_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Documento do Cliente CPF ou CNPJ' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_CPF_CNPJ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data de Cadastro do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DT_CADASTRO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Representante do Cliente' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NOME_REPRESENTANTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NOME_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao do Status do Cliente no Distribuidor, exe: Aprovado/Reprovado' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_STATUS_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/************************
*BI_B2B.[STG_DISTRIBUIDOR]*
************************/

USE [ADD_STAGING]
GO

/****** Object:  Table [bi_b2b].[STG_DISTRIBUIDOR]    Script Date: 12/12/2019 18:27:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [bi_b2b].[STG_DISTRIBUIDOR](
	[CD_DISTRIBUIDOR] [int] NOT NULL,
	[NOME_DISTRIBUIDOR] [varchar](8000) NULL,
	[DS_DISTRIBUIDOR] [varchar](8000) NULL,
	[DS_STATUS_DISTRIBUIDOR] [varchar](8000) NULL,
	[NUM_PRIORIDADE] [int] NULL,
	[NUM_CNPJ] [varchar](8000) NULL,
	[DS_EMAIL] [varchar](8000) NULL,
	[NUM_CEP_INICIO_RANGE] [varchar](8000) NULL,
	[NUM_CEP_FIM_RANGE] [varchar](8000) NULL,
	[NUM_TELEFONE_COM] [int] NULL,
	[NUM_TELEFONE_COM2] [int] NULL,
	[DS_TIPO_ORIGEM] [varchar](8000) NULL,
	[DT_INS] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CD_DISTRIBUIDOR] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'CD_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NOME_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao do Status do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_STATUS_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero da prioridade do distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_PRIORIDADE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Documento do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_CNPJ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'E-mail do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_EMAIL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero do CEP do Inicio do Range' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_CEP_INICIO_RANGE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero do CEP do fim do Range' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_CEP_FIM_RANGE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do telefone do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_TELEFONE_COM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número secundário do telefone do Distribuidor' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'NUM_TELEFONE_COM2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO





----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/***************************
*BI_B2B.[STG_ENDERECO_PEDIDO]*
***************************/

USE ADD_STAGING
GO

/****** Object:  Table BI_B2B.[STG_ENDERECO_PEDIDO]    Script Date: 11/12/2019 16:21:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--- Ver o que deu errado                         varchar(8000)
CREATE TABLE BI_B2B.[STG_ENDERECO_PEDIDO](
	
	[COD_PEDIDO] [int],
	[NUM_CEP_DO_CLIENTE] [varchar](8000),
	[DS_PAIS_DO_CLIENTE] [varchar](8000),
	[DS_ESTADO_DO_CLIENTE] [varchar](8000),
	[DS_UF_DO_CLIENTE] [varchar](8000),
	[DS_CIDADE_DO_CLIENTE] [varchar](8000),
	[DS_BAIRRO_DO_CLIENTE] [varchar](8000),
	[DS_LOGRADOURO_DO_CLIENTE] [varchar](8000),
	[NUM_ENDERECO_CLIENTE] [varchar](8000),
	[NUM_CEP_DA_ENTREGA] [varchar](8000),
	[DS_PAIS_DA_ENTREGA] [varchar](8000),
	[DS_ESTADO_DA_ENTREGA] [varchar](8000),
	[DS_UF_DA_ENTREGA] [varchar](8000),
	[DS_CIDADE_DA_ENTREGA] [varchar](8000),
	[DS_BAIRRO_DA_ENTREGA] [varchar](8000),
	[DS_LOGRADOURO_DA_ENTREGA] [varchar](8000),
	[NUM_ENDERECO_ENTREGA] [varchar](8000),
	[NUM_CEP_DO_FATURAMENTO] [varchar](8000),
	[DS_PAIS_DO_FATURAMENTO] [varchar](8000),
	[DS_ESTADO_DO_FATURAMENTO] [varchar](8000),
	[DS_UF_DO_FATURAMENTO] [char](8000),
	[DS_CIDADE_DO_FATURAMENTO] [char](8000),
	[DS_BAIRRO_DO_FATURAMENTO] [char](8000),
	[DS_LOGRADOURO_DO_FATURAMENTO] [char](8000),
	[NUM_ENDERECODO_FATURAMENTO] [char](8000),
	[DS_TIPO_ORIGEM] [varchar](8000),
	[DT_INS] [datetime],
	
PRIMARY KEY CLUSTERED 
(
	[COD_PEDIDO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo do Pedido do Cliente' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'COD_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Cep da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_CEP_DO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abreviação do País da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_PAIS_DO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Estado da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_ESTADO_DO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição UF da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_UF_DO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da cidade da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_CIDADE_DO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do bairro da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_BAIRRO_DO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Logradouro da realização do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_LOGRADOURO_DO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero do Logradouro da realização do Pedido, podendo conter string quando se tratar de apartamento' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_ENDERECO_CLIENTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Cep da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_CEP_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abreviação do País da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_PAIS_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Estado da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_ESTADO_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição UF da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_UF_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da cidade da entega do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_CIDADE_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do bairro da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_BAIRRO_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Logradouro da entrega do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_LOGRADOURO_DA_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero do Logradouro da entrega do Pedido, podendo conter string quando se tratar de apartamento' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_ENDERECO_ENTREGA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Cep da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_CEP_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abreviação do País da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_PAIS_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Estado da faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_ESTADO_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição UF da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_UF_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da cidade da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_CIDADE_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do bairro da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_BAIRRO_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Logradouro da Faturamento do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_LOGRADOURO_DO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero do Logradouro da Faturamento do Pedido, podendo conter string quando se tratar de apartamento' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'NUM_ENDERECODO_FATURAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_ENDERECO_PEDIDO', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*****************
*BI_B2B.[STG_FRONT]*
*****************/


USE ADD_STAGING
GO

/****** Object:  Table BI_B2B.[STG_FRONT]    Script Date: 03/12/2019 19:43:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE BI_B2B.[STG_FRONT](
	[SK_FRONT] [int],
	[CD_FRONT] [int],
	[DS_FRONT] [varchar](8000),
	[DS_TIPO_ORIGEM] [varchar](8000),
	[DT_INS] [datetime],
	
PRIMARY KEY CLUSTERED 
(
	[CD_FRONT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de Captacao refente ao Front do Cliente' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FRONT', @level2type=N'COLUMN',@level2name=N'CD_FRONT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao do Front' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FRONT', @level2type=N'COLUMN',@level2name=N'DS_FRONT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FRONT', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_FRONT', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*****************
**BI_B2B.[STG_KIT]*
*****************/

USE ADD_STAGING
GO

/****** Object:  Table BI_B2B.[STG_KIT]    Script Date: 04/12/2019 12:08:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE BI_B2B.[STG_KIT](
	[KIT_SKU_ID] [int],
	[KIT_SKU_NAME] [varchar](8000),
	[DS_TIPO_ORIGEM] [varchar](8000),
	[DT_INS] [datetime],
	
PRIMARY KEY CLUSTERED 
(
	[KIT_SKU_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo do Kit do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_KIT', @level2type=N'COLUMN',@level2name=N'KIT_SKU_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Kit do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_KIT', @level2type=N'COLUMN',@level2name=N'KIT_SKU_NAME'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_KIT', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_KIT', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*****************
**BI_B2B.[STG_LOJA]*
*****************/

USE ADD_STAGING
GO

/****** Object:  Table BI_B2B.[STG_LOJA]    Script Date: 22/11/2019 15:00:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE BI_B2B.[STG_LOJA](
	
	[CD_LOJA] [int],
	[NOME_LOJA] [varchar](8000),
	[CD_FILIAL] [int],
	[NOME_FILIAL] [varchar](8000),
	[DS_TIPO_ORIGEM] [varchar](8000),
	[DT_INS] [datetime],
	
PRIMARY KEY CLUSTERED 
(
	[CD_LOJA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'CD_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'NOME_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Filial por Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'CD_FILIAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da Filial' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'NOME_FILIAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO




----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/************************
**BI_B2B.[STG_NOTA_FISCAL]*
************************/

USE ADD_STAGING
GO

/****** Object:  Table BI_B2B.[STG_LOJA]    Script Date: 22/11/2019 15:00:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE BI_B2B.[STG_LOJA](
	
	[CD_LOJA] [int],
	[NOME_LOJA] [varchar](8000),
	[CD_FILIAL] [int],
	[NOME_FILIAL] [varchar](8000),
	[DS_TIPO_ORIGEM] [varchar](8000),
	[DT_INS] [datetime],
	
PRIMARY KEY CLUSTERED 
(
	[CD_LOJA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'CD_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'NOME_LOJA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Filial por Loja' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'CD_FILIAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da Filial' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'NOME_FILIAL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_LOJA', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*******************
**BI_B2B.[STG_PEDIDO]*
*******************/


USE ADD_STAGING
GO

/****** Object:  Table BI_B2B.[STG_PEDIDO]    Script Date: 22/11/2019 15:20:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE BI_B2B.[STG_PEDIDO](
	
	[CD_PEDIDO] [int],
	[DT_CRIACAO_PEDIDO] [date],
	[DS_TIPO_ORIGEM] [varchar](8000),
	[DT_INS] [datetime],

PRIMARY KEY CLUSTERED 
(
	[CD_PEDIDO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

------

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PEDIDO', @level2type=N'COLUMN',@level2name=N'CD_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da criação do Pedido' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PEDIDO', @level2type=N'COLUMN',@level2name=N'DT_CRIACAO_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PEDIDO', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*******************
*BI_B2B.[STG_PRODUTO]*
*******************/

USE ADD_STAGING
GO

/****** Object:  Table BI_B2B.[STG_PRODUTO]    Script Date: 22/11/2019 15:49:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE BI_B2B.[STG_PRODUTO](
	[CD_PRODUTO] [int],
	[CD_EAN] [int],
	[DS_NOME_PRODUTO] [varchar](8000),
	[DS_TITULO_PRODUTO] [varchar](8000),
	[DS_PRODUTO] [varchar](8000),
	[DS_URL_PRODUTO] [varchar](8000),
	[DS_COR] [varchar](8000),
	[DS_TAMANHO] [varchar](8000),
	[DS_CATEGORIA] [varchar](8000),
	[DS_GENERO] [varchar](8000),
	[DS_MARCA] [varchar](8000),
	[DS_FABRICANTE] [varchar](8000),
	[DS_DE_DIVISAO_DE_PRODUTO] [char](8000),
	[DS_FORNECEDOR] [varchar](8000),
	[DS_STATUS_PRODUTO] [varchar](8000),
	[DS_ESTACAO] [varchar](8000),
	[DS_CAMPANHA] [varchar](8000),
	[FLAG_MARKDOWN] [varchar](8000),
	[DS_TIPO_ORIGEM] [varchar](8000),
	[DT_INS] [datetime],

PRIMARY KEY CLUSTERED 
(
	[CD_PRODUTO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'CD_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do Ean' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'CD_EAN'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Nome Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_NOME_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Titulo do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_TITULO_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Url do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_URL_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Cor do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_COR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição  do Tamanho Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_TAMANHO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Categoria do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_CATEGORIA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Genero  do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_GENERO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Marca do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_MARCA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Fabricante do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_FABRICANTE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição de Divisão  do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_DE_DIVISAO_DE_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Fornecedor do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_FORNECEDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Status do Produto' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_STATUS_PRODUTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Estação em que o Produto foi comprado' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_ESTACAO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição se o Produto e referente a alguma campanha' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_CAMPANHA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_PRODUTO', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO




----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



/**************************
*BI_B2B.[STG_REGRA_PROMOCAO]*
**************************/


USE ADD_STAGING
GO

/****** Object:  Table BI_B2B.[STG_REGRA_PROMOCAO]    Script Date: 26/11/2019 17:23:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE BI_B2B.[STG_REGRA_PROMOCAO](
	
	[CD_REGRA] [int],
	[DS_REGRA] [varchar](8000),
	[CD_REGRA_TEMPLATE] [int],
	[DS_ORIGEM] [varchar](8000),
	[DT_CRIACAO] [datetime],
	[DT_EXPIRACAO] [datetime],
	[DS_TIPO_ORIGEM] [varchar](8000),
	[DT_INS] [datetime],

PRIMARY KEY CLUSTERED 
(
	[CD_REGRA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Regra de Promocão' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'CD_REGRA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Regra de Promoção' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'DS_REGRA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo da Regra de Promocão do Template' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'CD_REGRA_TEMPLATE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da Origem da Regra de Promoção' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'DS_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Criacao da Promocao' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'DT_CRIACAO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data que a Promocao expira' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'DT_EXPIRACAO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_REGRA_PROMOCAO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/***************************************
*BI_B2B.[STG_STATUS_CLIENTE_DISTRIBUIDOR]*
***************************************/

USE ADD_STAGING
GO

/****** Object:  Table BI_B2B.[STG_STATUS_CLIENTE_DISTRIBUIDOR]    Script Date: 22/11/2019 15:53:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE BI_B2B.[STG_STATUS_CLIENTE_DISTRIBUIDOR](
	[CD_STS_CLIENTE_DISTRIBUIDOR] [int],
	[DESCRICAO_STATUS_CLIENTE_DISTRIBUIDOR] [varchar](8000),
	[DS_TIPO_ORIGEM] [varchar](8000),
	[DT_INS] [datetime],
	
PRIMARY KEY CLUSTERED 
(
	[CD_STS_CLIENTE_DISTRIBUIDOR] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de Cliente por Distribuidor' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_STATUS_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'CD_STS_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_STATUS_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao dos Status  do Rastreio' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_STATUS_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DESCRICAO_STATUS_CLIENTE_DISTRIBUIDOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_STATUS_CLIENTE_DISTRIBUIDOR', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*************************
*BI_B2B.[STG_STATUS_PEDIDO]*
*************************/

USE [ADD_STAGING]
GO

/****** Object:  Table [bi_b2b].[STG_STATUS_PEDIDO]    Script Date: 12/12/2019 17:33:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [bi_b2b].[STG_STATUS_PEDIDO](
	[NOME_STATUS_PEDIDO] [varchar](8000) NULL,
	[DS_STATUS] [varchar](8000) NULL,
	[DS_TIPO_ORIGEM] [varchar](8000) NULL,
	[DT_INS] [datetime] NULL
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo do Status Pedido' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_STATUS_PEDIDO', @level2type=N'COLUMN',@level2name=N'NOME_STATUS_PEDIDO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao dos Status  do Rastreio' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_STATUS_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_STATUS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_STATUS_PEDIDO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'bi_b2b', @level1type=N'TABLE',@level1name=N'STG_STATUS_PEDIDO', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/************************
*BI_B2B.[STG_TIPO_ESTOQUE]*
************************/


USE ADD_STAGING
GO

/****** Object:  Table BI_B2B.[STG_TIPO_ESTOQUE]    Script Date: 26/11/2019 17:27:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE BI_B2B.[STG_TIPO_ESTOQUE](
	
	[CD_TIPO_ESTOQUE] [int],
	[DS_ESTOQUE] [varchar](8000),
	[DS_TIPO_ORIGEM] [varchar](8000),
	[DT_INS] [datetime],
	
PRIMARY KEY CLUSTERED 
(
	[CD_TIPO_ESTOQUE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo do Tipo de Estoque' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_TIPO_ESTOQUE', @level2type=N'COLUMN',@level2name=N'CD_TIPO_ESTOQUE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descricao do Estoque' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_TIPO_ESTOQUE', @level2type=N'COLUMN',@level2name=N'DS_ESTOQUE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_TIPO_ESTOQUE', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_TIPO_ESTOQUE', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/**************************
*BI_B2B.[STG_TIPO_PAGAMENTO]*
**************************/


USE ADD_STAGING
GO

/****** Object:  Table BI_B2B.[STG_TIPO_PAGAMENTO]    Script Date: 22/11/2019 15:58:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE BI_B2B.[STG_TIPO_PAGAMENTO](
	
	[CD_TIPO_PAGAMENTO] [int],
	[NOME_TIPO_PAGAMENTO] [varchar](8000),
	[DS_TIPO_PAGAMENTO] [varchar](8000),
	[DS_TIPO_BANDEIRA] [varchar](8000),
	[DS_TIPO_VOUCHER] [varchar](8000),
	[DS_TIPO_ORIGEM] [varchar](8000),
	[DT_INS] [datetime],

PRIMARY KEY CLUSTERED 
(
	[CD_TIPO_PAGAMENTO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código de Tipo do Pagamento' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'CD_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Tipo de Pagamento ' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_PAGAMENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Tipo de Bandeira' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_BANDEIRA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do Voucher Utilizado' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_VOUCHER'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da plataforma de Origem ' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'DS_TIPO_ORIGEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da Inserção do Registro' , @level0type=N'SCHEMA',@level0name=N'BI_B2B', @level1type=N'TABLE',@level1name=N'STG_TIPO_PAGAMENTO', @level2type=N'COLUMN',@level2name=N'DT_INS'
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

