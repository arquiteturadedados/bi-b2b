/*********************************************************************************************************************************
**********************************************************************************************************************************
*********************************************************************************************************************************/

-- QUERY PARA STG_DISTRIBUIDOR -- DIM_DISTRIBUIDOR


-- PEGAR TELEFONE POR CLIENTE, VERIFICAR SE SER� NECESS�RIO ADICIONAR O STORE_ID


CREATE  TEMPORARY TABLE   CLIENTE_POR_TELEFONE_2

SELECT 
	  		 EP.CUSTOMER_ID
  			,EP.PHONE_TP									AS PHONE_TP_0
  			,CONCAT(EP.AREA_CD,EP.PHONE_NR) 				AS FONE_TP_0
  			,EPE.PHONE_TP									AS PHONE_TP_1
  			,CONCAT(EPE.AREA_CD,EPE.PHONE_NR) 				AS FONE_TP_1
			,EPEE.PHONE_TP									AS PHONE_TP_2
  			,CONCAT(EPEE.AREA_CD,EPEE.PHONE_NR)				AS FONE_TP_2
  	   FROM ECSL_PHONE EP
 LEFT  JOIN				
	  		( SELECT 
			  		TEL.CUSTOMER_ID,
		  			TEL.PHONE_TP,
		  			TEL.AREA_CD,
		  			TEL.PHONE_NR 
		  	   FROM ECSL_PHONE TEL
			  WHERE TEL.PHONE_TP = 1
				AND TEL.CUSTOMER_ID  = 97200)EPE ON EPE.CUSTOMER_ID = EP.CUSTOMER_ID
 LEFT  JOIN				
	  		( SELECT 
			  		PHO.CUSTOMER_ID,
		  			PHO.PHONE_TP,
		  			PHO.AREA_CD,
		  			PHO.PHONE_NR 
		  	   FROM ECSL_PHONE PHO
			  WHERE PHO.PHONE_TP = 2
				AND PHO.CUSTOMER_ID  = 97200)EPEE ON EPEE.CUSTOMER_ID = EP.CUSTOMER_ID
			WHERE EP.PHONE_TP = 0
 			 AND EP.CUSTOMER_ID  = 97200
 			 
 			 
/*********************************************************************************************************************************
**********************************************************************************************************************************
*********************************************************************************************************************************/

 --  QUERY STAGE DIMENS�O CLIENTE			 
 			 			 
SELECT DISTINCT 
 EC.CUSTOMER_ID  																			AS CD_CLIENTE 							-- CODIGO DO CLIENTE
,UPPER(EC.NAME)																				AS NOME_CLIENTE 						-- NOME DO CLIENTE
,EC.REPRESENTATIVE_NM																		AS NOME_REPRESENTANTE					-- NOME DO REPRESENTANTE DO CLIENTE
,UPPER(EC.EMAIL)																			AS EMAIL_CLIENTE 						-- EMAIL CLIENTE
,ECG.NAME																					AS GRUPO_CLIENTE 						-- FIXAR B2B
-- ,FONE_TP_0																					AS NUM_TELEFONE							-- NECESSARIO ENTENDER O TP_TELEFONE PARA COLOCAR A INFORMACAO
-- ,FONE_TP_2																					AS NUM_TELEFONE_2						-- NECESSARIO ENTENDER O TP_TELEFONE PARA COLOCAR A INFORMACAO
,EC.DOCUMENT_NR																				AS CPF_CNPJ 							-- CPF OU CNPJ DO CLIENTE - PARA B2B = CNPJ E B2C = CPF
,CASE WHEN LENGTH(EC.DOCUMENT_NR)>=14 THEN 'CNPJ' ELSE 'CPF' END 							AS FLAG_CPF_CNPJ 						-- INDICA SE � PF OU PJ	
,EC.CREATE_DT																				AS DT_CADASTRO 							-- DATA CADASTRO QUANDO O CARA FOR CNPJ - QUANDO NULO INSERIR 1900-12-31
,EC.BIRTH_DT 																				AS DT_NASCIMENTO 						-- DATA NASCIMENTO QUANDO O CARA FOR CPF  - QUANDO NULO INSERIR 1900-12-31
,EC.GENDER																					AS DS_GENERO							-- GENERO DO SEXO
,'SOLTEIRO'																					AS DS_ESTADO_CIVIL						-- ESTADO CIVIL
,EA.POSTAL_CD																				AS NUM_CEP_CLIENTE 						-- CEP DO CLIENTE
,UPPER(EA.COUNTRY_ID) 																		AS PAIS_CLIENTE 						-- PAIS DO CLIENTE
,UPPER(EA.STATE)																			AS DS_UF								-- UF ESTADOS
,UPPER(EA.CITY)				    														    AS CIDADE_CLIENTE 						-- CIDADE DO CLIENTE
,UPPER(EA.QUARTER)			    														    AS BAIRRO_CLIENTE 						-- BAIRRO DO CLIENTE
,EA.ADDRESS_NR 																				AS ENDERECO_NUMERO 	     				-- NUMERO DO ENDERECO DO CLIENTE
,'2019-10-10'																				AS DT_INS	
,'2019-10-10'																				AS DT_UPD
FROM ECSL_CUSTOMER EC
JOIN ECSL_ADDRESS EA
  ON EC.MAIN_ADDRESS_ID = EA.ADDRESS_ID   
 AND EC.STORE_ID = EA.STORE_ID
 AND EC.CUSTOMER_ID = EA.CUSTOMER_ID
JOIN ECSL_CUSTOMER_GROUP  ECG		--
  ON EC.CUSTOMER_GROUP_ID = ECG.CUSTOMER_GROUP_ID
 AND EC.STORE_ID = ECG.STORE_ID     								-- phone tipo fone = 0
JOIN ECSL_PHONE  EP
  ON EC.CUSTOMER_ID = EP.CUSTOMER_ID
 AND EC.STORE_ID = EP.STORE_ID 
 WHERE 1 = 1  
--  AND EC.STORE_ID = 'UNILEVER' 
 -- AND ec.CUSTOMER_ID  IN ( 180200,180201) --ORACLE
 -- AND EC.CUSTOMER_ID  = 97200 --MARIADB
  
   
 			 
 			 