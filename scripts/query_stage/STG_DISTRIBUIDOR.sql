-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-- QUERY PARA STG_DISTRIBUIDOR -- DIM_DISTRIBUIDOR



			   SELECT
				   	A.WAREHOUSE_ID						AS COD_DISTRIBUIDOR, 				-- Codigo de Distribuidor
				   	A.WAREHOUSE_NAME					AS NOME_DISTRIBUIDOR,				-- Nome do Distribuidor
-- 				   	C.NAME								AS TIPO_DISTRIBUIDOR,				-- Necess�rio encontra a descri��o, so encontramos o Cod
				   	A.PRIORITY							AS NUM_PRIORIDADE,					-- Numero da Prioridade do Distribuidor
				   	A.CNPJ 								AS NUM_CNPJ,						-- N�mero do Documento do Distribuidor
				   	 'N/D'								AS DS_EMAIL,						-- Encontrar o email do distribuidor
 				MIN(B.RANGE_START)						AS NUM_CEP_INICIO_RANGE,			-- Numero inicial do CEP atendido pelo distribuidor
 				MAX(B.RANGE_END) 						AS NUM_CEP_FIM_RANGE,			    -- Numero final do CEP atendido pelo distribuidor
				   	 'ND'								AS NUM_TELEFONE_COM,				-- Encontrar o numero de telefone do distribuidor
				   	 'ND'								AS NUM_TELEFONE_COM2,				-- Encontrar o numero de telefone 2 do distribuidor
				   	A.DESCRIPTION						AS DS_DISTRIBUIDOR,					-- Descri��o do Distribuidor
				   	A.INS_DATE							AS DT_INS,							-- Data da Inser��o do Registro
				   	A.ALT_DATE							AS DT_UPD							-- Data da Atualiza��o do Registro
				FROM ifc.ECAD_WAREHOUSE 	A												-- TABELA COM INFORMA��ES GERAIS DO DISTRIBUIDOR
 		   LEFT JOIN ifc.WAREHOUSE_CEP      B												-- TABELA S� EXISTE NO IFC 
 				  ON A.WAREHOUSE_ID = B.WAREHOUSE_ID										
 				 AND A.STORE_ID = B.STORE_ID
-- 		   LEFT JOIN ifc.ECAD_WAREHOUSE_CLASSIFICATI_TP C									-- TABELA S� EXISTE NO ORACLE
-- 		   		  ON A.WAREHOUSE_CLASSIFICATION_TP_ID = C.WAREHOUSE_CLASSIFICATION_TP_ID
-- 		   		 AND A.STORE_ID = C.STORE_ID
 			   WHERE 1 = 1
			     AND A.STORE_ID = 'unilever' 
	-- 			 AND A.WAREHOUSE_ID = 2
			GROUP BY     
					A.WAREHOUSE_ID,
					A.WAREHOUSE_NAME,
					A.PRIORITY,
					A.DESCRIPTION,
					A.INS_DATE,
					A.ALT_DATE
					

					


