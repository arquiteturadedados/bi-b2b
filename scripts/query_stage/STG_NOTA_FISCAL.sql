-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-- QUERY PARA STG_NOTA_FISCAL -- DIM_NOTA_FISCAL                                                          
   
 	SELECT 
	 	   A.ORDER_ID						AS CD_PEDIDO								-- Numero do Pedido				
	 	   B.INVOICE_NUMBER					AS CD_NOTA_FISCAL,							-- Numero da Nota Fiscal
	 	   B.OCCURRENCE_DT					AS DT_CRIACAO_NOTA_FISCAL					-- Data Da Criacao da Nota Fiscal
	 	   '2019-20-11'						AS DT_INS									-- Data da Inser��o do Registro
	 	   '2019-20-11'						AS DT_UPD									-- Data da Atualiza��o do Registro
 	  FROM ifc.ECAD_ORDER  A
	  JOIN ECAD_SKU_ERP_DELIVERY_TRACKING B
	    ON CAST(CONCAT(A.ORDER_ID,'01')AS INT) = B.ERP_DELIVERY_ID
 	 WHERE 1 = 1 
  --   AND A.ORDER_ID = 10832679
  GROUP BY
 	 	   A.ORDER_ID,
 	 	   B.INVOICE_NUMBER,
 	 	   B.OCCURRENCE_DT
 	 	   
 	 	  
