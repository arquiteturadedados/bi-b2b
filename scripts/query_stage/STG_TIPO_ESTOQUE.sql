
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-- QUERY PARA STG_TIPO ESTOQUE -- DIM_TIPO_ESTOQUE


			SELECT
					STOCK_TP_ID 						AS  CD_TIPO_ESTOQUE,  						-- Codigo do Tipo de Estoque
					NAME								AS	NOME_TIPO_ESTOQUE,						-- Nome do Tipo de Estoque
					DESCRIPTION							AS	DS_TIPO_ESTOQUE,						-- Descricao do Tipo de Estoque
					SOURCE_NAME							AS 	DS_ORIGEM,								-- Descricao da Origem do Tipo de Estoque
					'2019-01-01'						AS 	DT_INS,									-- Data da Inser��o do Registro			
					'2019-01-01'						AS	DT_UPD									-- Data da Atualiza��o do Registro
			  FROM ifc.ECAD_STOCK_TP